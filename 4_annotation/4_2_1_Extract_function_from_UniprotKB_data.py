#!/usr/bin/env python
# This script extract information from uniprot_sprot.xml 
# for relevant Uniprot genes and makes two files:
#
# 1. "uniprot_function_subset_comments.tsv", which is more concise
# with lines like
# "14335_ORYSJ     Is associated with a DNA binding complex that binds
# to the G box, a well-characterized cis-acting DNA regulatory element
# found in plant genes.|Ubiquitous."
#
# 2. "uniprot_function_all_comments.tsv", which include all information
# with lines like
# "14335_ORYSJ     function:Is associated with a DNA binding complex that binds
# to the G box, a well-characterized cis-acting DNA regulatory element found
# in plant genes.|subcellular location:Cytoplasm|subcellular location:Nucleus|
# tissue specificity:Ubiquitous.|induction:By wounding, drought and salt stresses,
# benzothiadiazole (BTH), ethephon, methyl jasmonate (MeJa), hydrogen peroxide,
# abscisic acid (ABA) and incompatible and compatible races of
# rice blast fungus (M.grisea) and rice bacterial blight (X.oryzae).|
# similarity:Belongs to the 14-3-3 family."


import gzip
from os import access

#import xmlschema # install: `pip install xmlschema`
import xml.etree.ElementTree as ET

# which columns we want to get from the xml file
# in the schema in https://www.uniprot.org/docs/uniprot.xsd
# there are all the comment types
#    "allergen"
#    "alternative products"
#    "biotechnology"
#    "biophysicochemical properties"
##    "catalytic activity"
#    "caution"
##    "cofactor"
##    "developmental stage"
#    "disease"
##    "domain"
#    "disruption phenotype"
#    "activity regulation"
#    "function"
#    "induction"
#    "miscellaneous"
#    "pathway"
#    "pharmaceutical"
#    "polymorphism"
#    "PTM"
#    "RNA editing"
#    "similarity"
#    "subcellular location"
#    "sequence caution"
#    "subunit"
#    "tissue specificity"
#    "toxic dose"
#    "online information"
#    "mass spectrometry"
#    "interaction"

input_dir = '/home/qw254/projects/public_reference_data/uniprot/Release_2022_01/' # on cluster
entry_file = input_dir+'uniprot_sprot.xml'
output_dir = '/home/qw254/projects/Lv_Am/annotation/'
output_file = output_dir+'uniprot_function_all_comments.tsv'
#output_file2 = output_dir+'uniprot_function_only_accession.tsv'

# There are two versions of output under work_dir
#
# 1. uniprot_function_subset_comments.tsv contains only three types of comments:
# this version include line 107
# - 'function'
# - "tissue specificity"
# - "developmental stage":
#
# 2. uniprot_function_all_comments.tsv
#       contains all comments

def Check_gene_name(str):
    # check whether str is a valid UniProt name
    # Check_gene_name('ACKR3_HUMAN')
    # https://www.uniprot.org/help/entry_name
    # The UniProtKB/Swiss-Prot entry name consists of up to
    #   11 uppercase alphanumeric characters with a naming convention
    #   that can be symbolized as X_Y, where:
    # X is a mnemonic protein identification code of
    #   at most 5 alphanumeric characters;
    # The ‘_’ sign serves as a separator;
    # Y is a mnemonic species identification code of
    #   at most 5 alphanumeric characters.
    EMPTY=''
    s=str.strip().split('_')
    if len(s)!=2:
        return EMPTY
    X=s[0]
    Y=s[1]
    if len(X)>5:
        return EMPTY
    if len(Y)>5:
        return EMPTY
    return str



fout=open(output_file,'w')
#fout2=open(output_file2,'w')

gene_name=''
function=''
last_event=''
last_tag_name=''
for event, elem in ET.iterparse(entry_file):
    if event=='end':
        tag=elem.tag # e.g. elem.tag as '{http://uniprot.org/uniprot}accession'
        tag_name=tag.split('}')[1] # process to make it more readable
        #print(tag_name)
        if tag_name=='name': # entry name or other names
            info = Check_gene_name(elem.text)
            if len(info)>0: # if it is a valid uniprot name
                if last_tag_name=='accession':
                    # double check that gene_name always follow accession
                    # example:
                    # <accession>Q9CX69</accession>
                    # <name>FLOWR_MOUSE</name>
                    gene_name=info
                    fout.write('\n'+gene_name+'\t'+accession_id)
                #else: # not really gene name, continue; real gene name should be right after accession
                #    print('strange situation, last_tag_name is', last_tag_name,'not accession')
                #    print(elem.text, elem.tag)
                #    break
            #else: 
            #    print('elem.text/Check_name',elem.text)
        elif tag_name=="comment":
            if 'type' in elem.attrib:
                #if elem.attrib["type"]=='function' or elem.attrib["type"]=="tissue specificity" or elem.attrib["type"]=="developmental stage":
                if True: # write all comments for this gene
                    for sub_element in elem.iter():
                        for el in sub_element:
                            if el.text and len(el.text.strip())>0:
                                function=elem.attrib["type"]+':'+el.text
                                if 'interaction' in function: # too many items and uninformative
                                    continue
                                if len(gene_name)>0: # first time writing this gene function
                                    #output_content='\n'+gene_name+'\t'+accession_id+'\t'+function
                                    output_content='\t'+function
                                    fout.write(output_content)
                                    function=''
                                    gene_name=''
                                    accession_id=''
                                    output_content=''
                                    #print(output_content)
                                else: # another function annotation for the same gene
                                    fout.write('|'+function)
                                    #print('|'+function)
        elif tag_name=="accession":
            accession_id=elem.text
        last_tag_name=tag_name


fout.write('\n')
fout.close()
#fout2.close()