#!/bin/bash

# This script extract information from uniprot_sprot.xml 
# for gene lists of interst
python 4_2_1_Extract_function_from_UniprotKB_data.py

# In GO analysis, some enrichment shows up because of one gene counted three
# times, when the transcripts from one gene are considered as 'three genes'.
# To overcome this, we decide to collapse transcripts to genes, based on mapping
# to swissProt genes.
# So different corest genes will be collapsed to one gene, if they map (best
# match) to the same swissProt gene.
python 4_2_2_corset_2_swiss_prot_id.py








