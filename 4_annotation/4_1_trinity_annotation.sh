#!/bin/bash
# This script annoates the assembled transcriptome with trinotate
# https://github.com/Trinotate/Trinotate.github.io/wiki

# Lv
bash 4_1_1_trinotate_Lv.sh

# Am
bash 4_1_2_trinotate_Am.sh

# Note: the difference for Lv and Am scripts are that
# in Am scripts, there is also blast to the Am reference genome
# and this result is included in the final report