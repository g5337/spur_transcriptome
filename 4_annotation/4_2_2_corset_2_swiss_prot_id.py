#!/usr/bin/env python
# In GO analysis, some enrichment shows up because of one gene counted three
# times, when the transcripts from one gene are considered as 'three genes'.
# To overcome this, we decide to collapse transcripts to genes, based on mapping
# to swissProt genes.
# So different corest genes will be collapsed to one gene, if they map (best
# match) to the same swissProt gene.

# 1st step:
# define the best matching swissProt gene per corset gene (if there is no hit, don't write/ouput anything)
# output example line:
# "Cluster-10.0  PER64_ARATH"

# This script makes two files for Lv and Am respectively
# 1. "corset_id_2_swiss_prot_Lv_corset_point5.tsv"
# the line is like
# "Cluster-3.0     DHEA_NICPL"
# 2. "corset_id_2_swiss_prot_Am_corset_point5.tsv"
# the line is like
# "Cluster-22292.0 RIC7_ARATH"

import csv
import scipy.stats as stats

def Write_result(out_file,d,d_corset_id):
    fout=open(out_file,'w')
    for gid in d_corset_id.keys():
        if gid==None:
            continue
        if gid not in d:
            fout.write(gid+'\tNA'+'\n')
        else:
            content=d[gid][0]
            fout.write(gid+'\t'+content+'\n')
    fout.close()


def Build_dict_trinity_isoform_2_corset(infile):
    #infile='/home/qw254/corset/Lv/corset/Lv_trinity-clusters-0.5.txt' # TRINITY_DN67332_c0_g1_i2        Cluster-0.0
    d={}
    d_corset_id={}
    f=open(infile)
    for l in f:
        ls=l.strip().split('\t')
        isoform_id=ls[0]
        corset_id=ls[1]
        d[isoform_id]=corset_id
        d_corset_id[corset_id]=None
    return [d,d_corset_id]


def Return_d_isoform_2_uniprot(inputfile):
    dict={}
    f = open(inputfile)
    for l in f:
        ls = l.strip().split('\t')
        isoform_pro_id = ls[0]
        swiss_prot_id = ls[1]
        bitScore=float(ls[11]) # http://www.metagenomics.wiki/tools/blast/blastn-output-format-6
        dict[isoform_pro_id] = [swiss_prot_id,bitScore]
    f.close()
    return(dict)


## define files
# option 1:
#SP='Lv'
#file_SP_isoform_2_uniprot_blastp = '/home/qw254/programs/trinotate/Lv_full/Trinity_L_vulgaris_full.fasta.blastp.outfmt6'
#file_SP_isoform_2_uniprot_blastx = '/home/qw254/programs/trinotate/Lv_full/Trinity_L_vulgaris_full.fasta.blastx.outfmt6'

# option 2:
SP='Am'
file_SP_isoform_2_uniprot_blastp = '/home/qw254/programs/trinotate/Am/Trinity.fasta.blastp.outfmt6'
file_SP_isoform_2_uniprot_blastx = '/home/qw254/programs/trinotate/Am/Trinity.fasta.blastx.outfmt6'


# read files
d_SP_isoform_2_uniprot_blastp = Return_d_isoform_2_uniprot(file_SP_isoform_2_uniprot_blastp)
d_SP_isoform_2_uniprot_blastx = Return_d_isoform_2_uniprot(file_SP_isoform_2_uniprot_blastx)

corset_file='/home/qw254/programs/corset/'+SP+'/corset/'+SP+'_trinity-clusters-0.5.txt'

[d_isoform_2_corset,d_corset_id]=Build_dict_trinity_isoform_2_corset(corset_file)
# >>> next(iter(d_corset_id.items()))
# ('Cluster-0.0', None)
# >>> d_isoform_2_corset['TRINITY_DN4277_c0_g2_i2']
# 'Cluster-57773.0'

# generate dictionary
# d_corset_id_2_swissprot[corset_id][swiss_prot_id] = bitscore
d_corset_id_2_swissprot = {}
# load all information
for d_isoform_2_swissprot in [d_SP_isoform_2_uniprot_blastp,d_SP_isoform_2_uniprot_blastx]:
    for isoform_prot_id in  d_isoform_2_swissprot.keys():
        [swiss_prot_id,bitscore] = d_isoform_2_swissprot[isoform_prot_id]
        isoform_nt_id = isoform_prot_id.split('.')[0]
        if isoform_nt_id not in d_isoform_2_corset: # this transcript does belong to any corset id
            continue
        corset_id = d_isoform_2_corset[isoform_nt_id]
        if corset_id not in d_corset_id_2_swissprot:
            d_corset_id_2_swissprot[corset_id]={}
        if swiss_prot_id not in d_corset_id_2_swissprot[corset_id]:
            d_corset_id_2_swissprot[corset_id][swiss_prot_id] = bitscore
        else: # swiss_prot_id exist, keep the highest score
            maxscore = d_corset_id_2_swissprot[corset_id][swiss_prot_id] # the current maximal score
            if bitscore>maxscore:
                d_corset_id_2_swissprot[corset_id][swiss_prot_id] = bitscore


# select the swissProt id with the highest bitScore
d_corset_id_2_best_swissprot = {}
for corset_id in d_corset_id_2_swissprot:
    if corset_id not in d_corset_id_2_best_swissprot: # set it up
        d_corset_id_2_best_swissprot[corset_id] = {}
        max_bitscore = 0
    else:
        print('error...key in d_corset_id_2_swissprot has duplicates? check!')
    for swiss_prot_id in d_corset_id_2_swissprot[corset_id]:
        bitscore = d_corset_id_2_swissprot[corset_id][swiss_prot_id]
        if bitscore > max_bitscore:
            d_corset_id_2_best_swissprot[corset_id] = [swiss_prot_id,bitscore]
            max_bitscore=bitscore

#>>> len(d_corset_id)
#74868
#>>> len(d_corset_id_2_swissprot)
#40706
#>>> len(d_corset_id_2_best_swissprot)
#40706

GO_outfile='/home/qw254/projects/Lv_Am/corset_id_2_swiss_prot/corset_id_2_swiss_prot_'+SP+'_corset_point5_2023.tsv'
Write_result(GO_outfile,d_corset_id_2_best_swissprot,d_corset_id)
