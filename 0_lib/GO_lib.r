# This R script provides basic functions for GO enrichment analysis
# as well as load the basic data (in PROJECT_DIR and GO_DIR) required for GO analysis

library(topGO)

create_dir_if_not_exist <- function(dir) {
  # This function creates dir only when it doesn't exist
  if ( !dir.exists(dir)) {
    dir.create(dir,recursive = TRUE)
  }
}

Write_table_corset_2_swissprotID_4_for_stage <- function(gene_subset_name,corset_id_list){
  # This function takes corset_id_list and write this corset gene list to a file 
  # whose name contains gene_subset_name
  write.table(corset2swissprot[corset_id_list],paste0(OUT_DIR_TABLES,gene_subset_name,'_corsetID_2_swissprot.csv'),col.names = F, quote = F,sep = '\t')
}

Return_GO_enrichment_table <- function(myInteresting_corset_genes, ontology_class, node_size){
  # This function take myInteresting_corset_genes (a list of corset genes), map it to swissprot genes
  # and performed GO enrichment analysis for ontology_class with parameter node_size
  # it returns a named list with gene_list and GO results
  myInterestingGenes <- as.character(unique(corset2swissprot[myInteresting_corset_genes]))
  geneList <- factor(as.integer(geneNames %in% myInterestingGenes))
  names(geneList) <- geneNames
  ## build obj for enrichment analysis
  GOdata_early <- new("topGOdata", 
                      ontology = ontology_class,
                      allGenes = geneList,
                      annot = annFUN.gene2GO, 
                      gene2GO = geneID2GO,
                      nodeSize = node_size)
  ## enrichment anlysis
  resultFis <- runTest(GOdata_early, algorithm = "classic", statistic = "fisher")
  #showSigOfNodes(GOdata_early, score(resultFis), firstSigNodes = 5, useInfo = 'all')
  allRes_spur <- GenTable(GOdata_early, classic = resultFis,orderBy = "weight", ranksOf = "classic", topNodes = 1000)
  named_list = list(GO_result = allRes_spur, gene_list = myInteresting_corset_genes, GO_data = GOdata_early, GO_resultFis = resultFis )
  return(named_list)
}

Return_sig_GO_IDs <- function(allRes,p_value_cutoff){
  # This function takes the GO enrichment result and returns GO_IDs whose 
  # p-value is less than the p_value_cutoff
  return(allRes$GO.ID[which(allRes$classic < p_value_cutoff)])
}

Return_sig_GO_terms <- function(allRes,p_value_cutoff){
  # This function takes the GO enrichment result and returns GO_terms whose
  # p-value is less than the p_value_cutoff
  return(allRes$Term[which(allRes$classic < p_value_cutoff)])
}

Take_ratio <- function(x,y){ 
  # This function returns a more conserved estimate of the ratio
  # given both x and y are larger than 1
  # This also avoid the error from division by zero when y is 0
  return((x + 0.1)/(y + 0.1))
}

Return_GO_terms_with_highest_enrichment <- function(GO_info){
  # In case there are more than one GO terms whose p-values 
  # are lower than the cutoff, return the GO ID(s) with the lowest p-value
  GO_info$classic = as.numeric(GO_info$classic)  
  lowest_pvalue = min(GO_info$classic)
  return(GO_info$GO.ID[which(GO_info$classic <= lowest_pvalue)])
}

Write_GO_terms_from_same_gene_set <- function(res,gene_list_name,p_value_cutoff) {
# If there are one gene or one set of genes making the different GO pathways show up, 
# it may make the heatmap too busy/large, here we remove such redundant by
# return the GO_list with redundancy (one set of genes causing multiple GO terms) removed
# where only the GO_term with the lowest classic p-value is kept
# Note: the output directory OUT_DIR_TABLES is coded in the main script
# TODO: If one set of genes make different GO pathway show up, but the GO pathways 
# are not child-parent(including grandparents etc.) relationship, it may still worth keeping those
# so that we don't potentially remove interesting GO terms
  # first step: only process those terms that meet the p-value cutoff
  index_filtered = which(res$GO_result$classic <= p_value_cutoff)
  if (length(index_filtered) == 0) {
    return_obj = list(IDs = c(),terms = c())
    return(return_obj)
  }
  res$GO_result = res$GO_result[index_filtered,] # filter by p-value to keep only the significant ones
  GO_list = res$GO_result[,1] # GO terms to process
  GO_ID_without_redundancy = c()
  outfile_pre_dir = paste0(OUT_DIR_TABLES,gene_list_name,'/')
  create_dir_if_not_exist(outfile_pre_dir)
  outfile_pre = paste0(outfile_pre_dir,gene_list_name)
  # GO_to_remove_file = paste0(outfile_pre,'_GO_to_remove.txt') # make empty to remove file if not exist
  # if ( !file.exists(GO_to_remove_file)) {
  #   cat(paste('GO terms to remove from heatmap for', gene_list_name,'\n'),file = GO_to_remove_file)
  # }
  ## dictionary 
  gene_list_2_GO_dict =  list()
  for (GO_id in GO_list) {
    all_gene_list = genesInTerm(res$GO_data,GO_id)[[1]]
    score_list = scoresInTerm(res$GO_data,GO_id)[[1]]
    sel_gene_list = all_gene_list[which(score_list == 2)]
    index_name = paste(sel_gene_list,collapse = ' ')
    #message(index_name)
    #message(GO_id)
    #message('length ',length(gene_index))
    gene_list_2_GO_dict[index_name] = paste(unlist(unname(gene_list_2_GO_dict[index_name])),GO_id)
  }
  # write to a table if one gene set cause multiple tables
  N = length(gene_list_2_GO_dict)
  gene_name_list = names(gene_list_2_GO_dict)
  for (i in 1:N) {
    gene_names = gene_name_list[i]
    GO_str = gene_list_2_GO_dict[[i]]
    GO_terms = unlist(str_split(GO_str,' '))
    n = length(GO_terms)
    GO_terms = GO_terms[2:n] # removing the heading empty string
    # write gene list with GO_ID as filename
    for(GO_term in GO_terms) {
      outfile = paste0(outfile_pre,'_',GO_term,'.txt')
      cat(paste0(gene_names,'\n'),file = outfile)
    }
    # filter if there is redundancy
    if (n <= 1) {
      GO_ID_without_redundancy = c(GO_ID_without_redundancy,GO_terms)
      next
    }
    # > GO_terms
    # [1] ""           "GO:0042547"
    row_index = which(res$GO_result$GO.ID %in% GO_terms)
    GO_info = res$GO_result[row_index,]
    GO_terms_with_highest_enrichment = Return_GO_terms_with_highest_enrichment(GO_info)
    GO_ID_without_redundancy = c(GO_ID_without_redundancy,GO_terms_with_highest_enrichment)
    #message(gene_names)
    #message(GO_terms)
    outfile = paste0(outfile_pre,'_',i,'.txt')
    cat(paste0(gene_names,'\n'),file = outfile)
    write.table(GO_info, outfile,sep = "\t",append = TRUE, row.names = F, quote = F)
  }
  ## obtain GO terms from GO ID
  row_index = match(GO_ID_without_redundancy,res$GO_result$GO.ID)
  GO_terms_without_redundancy = res$GO_result$Term[row_index]
  ## construct obj to return 
  return_obj = list(IDs = GO_ID_without_redundancy,terms = GO_terms_without_redundancy)
  return(return_obj)
}


# input data directory
GO_DIR = paste0(PROJECT_DIR,'GO_enrichment/')
geneID2GO_file = paste0(GO_DIR,'swiss_prot_id_2_GO.tsv')
corset2swissprot_file = paste0(GO_DIR,'corset_id_2_swiss_prot_Lv_corset_point5.tsv')

# in this file, every corset id is associated with a list of GO terms when available
#the file is organised as cluster ID  number GO terms as characters various GO terms
# here the species A. majus is specified


## build data
#takes the number of GO terms as characters. Matches each Cluster ID to a GO term.
geneID2GO <- readMappings(file = geneID2GO_file)
geneNames <- names(geneID2GO)

## read prot annotation to corset genes
# > corset2swissprot['Cluster-819.0']
# Cluster-819.0 
#  Y4374_ARATH 
corset2swissprot_dat = read.table(corset2swissprot_file,sep = '\t',header = F)
corset2swissprot = corset2swissprot_dat[,2]
names(corset2swissprot) = corset2swissprot_dat[,1]

