library(stringr)

Rename_replicate_rA<-function(replicates){
  temp = paste(replicates,collapse = ':')
  temp = str_replace_all(temp,"2","A")
  temp = str_replace_all(temp,"3","B")
  temp = str_replace_all(temp,"4","C")
  temp = str_replace_all(temp,"5","D")
  temp = str_split(temp,':')[[1]]
  return(temp)
}

Rename_samples_rA<-function(temp, removeSPname=T){
  temp = paste(temp,collapse = ':')
  if(removeSPname){
    temp = str_replace_all(temp,"Am","")
    temp = str_replace_all(temp,"Lv","")
  }
  temp = str_replace_all(temp,"4_5","Inter")
  temp = str_replace_all(temp,"8_9","Early")
  temp = str_replace_all(temp,"_5V"," V rD")
  temp = str_replace_all(temp,"_4V"," V rC")
  temp = str_replace_all(temp,"_3V"," V rB")
  temp = str_replace_all(temp,"_2V"," V rA")
  temp = str_replace_all(temp,"_5D"," D rD")
  temp = str_replace_all(temp,"_4D"," D rC")
  temp = str_replace_all(temp,"_3D"," D rB")
  temp = str_replace_all(temp,"_2D"," D rA")
  temp = str_replace_all(temp,"2 ","Late ")
  temp = str_split(temp,':')[[1]]
  return(temp)
}


Rename_samples_R2<-function(temp){
  temp = paste(temp,collapse = ':')
  temp = str_replace_all(temp,"Am","")
  temp = str_replace_all(temp,"Lv","")
  temp = str_replace_all(temp,"4_5","Inter")
  temp = str_replace_all(temp,"8_9","Early")
  temp = str_replace_all(temp,"_5V"," V R5")
  temp = str_replace_all(temp,"_4V"," V R4")
  temp = str_replace_all(temp,"_3V"," V R3")
  temp = str_replace_all(temp,"_2V"," V R2")
  temp = str_replace_all(temp,"_5D"," D R5")
  temp = str_replace_all(temp,"_4D"," D R4")
  temp = str_replace_all(temp,"_3D"," D R3")
  temp = str_replace_all(temp,"_2D"," D R2")
  temp = str_replace_all(temp,"2 ","Late ")
  temp = str_split(temp,':')[[1]]
  return(temp)
}
