# This script does similar things as 0_lib/prep_4_spur_gene_1to1_match_only.r
# except in this one TPM table is from Am_ref (dds is still based on Am_trinity)

library(DESeq2)
library(ggplot2)
library(gridExtra)
library(grid)
library(plyr)

# load function libraries
home_dir = '~/'
lib_dir = paste0(home_dir,'gitlab/spur_transcriptome/0_lib/')  # where the sourced scripts are
source(paste0(lib_dir,'spur_gene_lib.r'))
source(paste0(lib_dir,'plot_gene_lib.r'))

# define species reference in the analysis
# For SpeA only 'Lv_trinity' is allowed
# For SpeB 'Am_trinity' is extensively tested, 'Am_ref' only works for some functions
SpeA = 'Lv_trinity'
SpeB = 'Am_ref' # alternative is 'Am_trinity'


# working directories for loading data, as well as root for output
WORK_DIR = paste0(home_dir,'/projects/Lv_Am/') # project directory

# First load dds data fro Lv and Am

IN_DIR = paste0( WORK_DIR,'result/R_obj/')

infile = paste0(IN_DIR,"Lv_dds_no_batch.rds")
Lv_dds = readRDS(infile)

# TODO: make it flexible interms of loading Am_trinity or Am_ref
infile = paste0(IN_DIR,"Am_dds_no_batch_outlier_removed.rds")
Am_dds = readRDS(infile)


# Load TPM data in to Lv_tpm_data,Am_tpm_data, AmRef_tpm_data
source(paste0(lib_dir,'Load_Lv_Am_TPM_data.R')) # TODO: make it flexible interms of loading Am_trinity or Am_ref


#  Load Lv to Am ortho table and use it to match the two TPM tables
## Note: here only ortho pairs in ortho_table are included in the ortho table

ORTHOLOG_DIR = paste0(WORK_DIR,'/result/ortholog/')
#scp qw254@hydrogen.plantsci.cam.ac.uk:/home/qw254/Lv_Am/Ortholog/* /home/qw254/projects/Lv_Am/result/ortholog

# read all pairs with blast information that include all orthologues, not only the 121_strict ones
#all_ortho_pairs_table = read.table(paste0(ORTHOLOG_DIR,"Orthofinder_Sep13_Lv_all_corset_genes_ortho_Lv_trinity_",SpeB,"_pairs.txt"),sep='\t',header=T,stringsAsFactors=F)

#ortho_table_all=read.table(paste0(ORTHOLOG_DIR,"Orthofinder_Sep13_Lv_all_corset_genes_ortho_Lv_trinity_",SpeB,"_pairs.txt"),sep='\t',header=T,stringsAsFactors=F)
#> dim(ortho_table_all)
#[1] 80629     5

# only the strict one to one match orthologues, only with unique 1 to 1 match, cbind works
ortho_table_strict = read.table(paste0(ORTHOLOG_DIR,"Orthofinder_Sep13_Lv_all_corset_genes_ortho_Lv_trinity_",SpeB,"_pairs_121_strict.txt"),sep='\t',header=T,stringsAsFactors=F)
#> dim(ortho_table_strict)
#[1] 19950     2

# this includes additional ones where in case of multiple match, the top score one is taken, if there are multiple top ones, 'MATCH' is written
#ortho_table_topScore=read.table(paste0(ORTHOLOG_DIR,"Orthofinder_Sep13_Lv_all_corset_genes_ortho_Lv_trinity_",SpeB,"_pairs_121_topScore.txt"),sep='\t',header=T,stringsAsFactors=F)
#> dim(ortho_table_topScore)
#[1] 3943    2

# merge the two lists unless there is 'MATCH'
ortho_table = ortho_table_strict #rbind(ortho_table_strict,ortho_table_topScore[which(ortho_table_topScore$Am_corset_id!='MATCH'),])

rownames(ortho_table) = ortho_table$Lv_corset_id
Lv_tpm_ortho = Lv_tpm_data[ortho_table$Lv_corset_id,]
Am_tpm_ortho = AmRef_tpm_data[ortho_table$Am_corset_id,]
tpm_ortho_table = cbind(Lv_tpm_ortho,Am_tpm_ortho) # now the row names are Lv_id

# all comparision and stats will be done in the log2 space
tpm_ortho_table_log2 = log2(tpm_ortho_table) # global variable

# load annotation from trinotate
source(paste0(lib_dir,'Load_Lv_Am_annotation.R'))

# preparation to plot candidate genes

# reformat the data to prepare for plotting
Lv_tpm_data_plot = t(Lv_tpm_data)
Am_tpm_data_plot = t(Am_tpm_data)
AmRef_tpm_data_plot = t(AmRef_tpm_data)

##### load data for checking where Am ortholog for a Lv_id exist in Am_trinity/Am_ref ###
Spe_Am = 'Am_trinity'
all_ortho_table_Lv_2_Am_trinity = read.table(paste0(ORTHOLOG_DIR,"Orthofinder_Sep13_Lv_all_corset_genes_ortho_Lv_trinity_",Spe_Am,"_pairs.txt"),sep = '\t',header=T,stringsAsFactors=F)


Spe_Am = 'Am_ref'
all_ortho_table_Lv_2_Am_ref = read.table(paste0(ORTHOLOG_DIR,"Orthofinder_Sep13_Lv_all_corset_genes_ortho_Lv_trinity_",Spe_Am,"_pairs.txt"),sep = '\t',header = T,stringsAsFactors = F)

# defined biological groups for TPM table for comparison and statistics
# including remove two outliers 2_D_rep5_Lv and Am2_4D
# Note: for DESeq2 only Am2_4D has been removed from its dds object

# For Lv_trinity
# remove one outlier sample X2_D_rep5_Lv
Lv_V_early=c("Lv8_9_2V","Lv8_9_3V","Lv8_9_4V","Lv8_9_5V")
Lv_D_early=c("Lv8_9_2D","Lv8_9_3D","Lv8_9_4D","Lv8_9_5D")

Lv_V_mid=c("Lv4_5_2V","Lv4_5_3V","Lv4_5_4V","Lv4_5_5V")
Lv_D_mid=c("Lv4_5_2D","Lv4_5_3D","Lv4_5_4D","Lv4_5_5D")

Lv_V_late=c("Lv2_2V","Lv2_3V","Lv2_4V","Lv2_5V")
Lv_D_late=c("Lv2_2D","Lv2_3D","Lv2_4D") #,"Lv2_5D")


# The biological groups are defined for later TPM comparison
## in functions Compare_TPM_two_conditions_gene and Compare_TPM_two_conditions_gene_list
# remove one outlier sample Am2_4D

Am_V_early=c("Am8_9_2V","Am8_9_3V","Am8_9_4V","Am8_9_5V")
Am_D_early=c("Am8_9_2D","Am8_9_3D","Am8_9_4D","Am8_9_5D")

Am_V_mid=c("Am4_5_2V","Am4_5_3V","Am4_5_4V","Am4_5_5V")
Am_D_mid=c("Am4_5_2D","Am4_5_3D","Am4_5_4D","Am4_5_5D")

Am_V_late=c("Am2_2V","Am2_3V","Am2_4V","Am2_5V")
Am_D_late=c("Am2_2D","Am2_3D","Am2_5D") #"Am2_4D") # removed

create_dir_if_not_exist <- function(dir) {
  # create dir only when it doesn't exist
  if ( !dir.exists(dir)) {
    dir.create(dir,recursive = TRUE)
  }
}

