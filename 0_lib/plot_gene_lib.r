# This script provided functions to prepare for plotting TPM of candidate genes
# which are used in
# 0_lib/prep_4_spur_gene_1to1_match_only_Am_ref.r:source(paste0(lib_dir,'plot_gene_lib.r'))
# 0_lib/prep_4_spur_gene_1to1_match_only.r:source(paste0(lib_dir,'plot_gene_lib.r'))

# global variable the functions depends on 
alpha = 0.05 # the CI is (1-alpha)

Dot_plot_tpm_gene_X <- function(geneX,tpm_data,conditionX,samples_to_remove = NULL){
  # Dot plot for TPM in groups
  # parameters
  #   geneX: gene ID
  #   tpm_data: tpm_data matrix
  #   conditionX: a list of biological groups for all samples in tpm_data matrix
  # return:
  #   ggplot ojb for geneX where condition in tpm_data is annotated by conditionX
  # example:
  #   Lv_id="Cluster-50976.0"
  #   outlier_sample = 'X2_V_rep5_Lv'
  #   Dot_plot_tpm_gene_X(Lv_id, Lv_tpm_data_plot, Lv_condition_plot, c(outlier_sample))
  #   Am_id = "Cluster-50976.0"
  #   outlier_sample = "X2_D_rep4_Am"
  #   Dot_plot_tpm_gene_X(Am_id, Am_tpm_data_plot, Am_condition_plot, c(outlier_sample))
  #
  # construct ggplot data obj
  tpm_geneX = as.numeric(as.character(tpm_data[, geneX]))
  plot_dat = data.frame(TPM = tpm_geneX, Condition = conditionX)
  # remove outlier before plotting
  if (!empty(samples_to_remove)) {
    outlier_index = which(rownames(tpm_data) %in%  samples_to_remove)
    plot_dat = plot_dat[-c(outlier_index),]
  }
  # plot
  p <- ggplot(plot_dat, aes(x = Condition, y = TPM),) + 
    geom_dotplot(binaxis = 'y', stackdir = 'center') +
    labs(title = geneX) +
    ylab('TPM') + 
    theme(text = element_text(size = 20), 
          axis.text.x = element_text(angle = 45, hjust=1),
          plot.title = element_text(size = 15)
    )
  return(p)
}


#### the following function to plot the mean and SE of biological groups for all groups
Return_y_max_min <- function(plot){
  # calcualte the position for the error bar
  # since there are less than 3 samples per biological group, it is ok to use a more general estiamtes
  vector_size = 3 # four sample per replicate # TODO: sometimes it is 3 samples, need revision
  alpha = 0.05 # the CI is (1-alpha)
  # https://www.r-graph-gallery.com/4-barplot-with-error-bar.html
  t_4_error_bar = qt((1 - alpha)/2 + .5, vector_size - 1)
  MAX = max(plot$data$col_mean + t_4_error_bar*plot$data$col_se)
  MIN = min(plot$data$col_mean - t_4_error_bar*plot$data$col_se)
  return(c(MIN,MAX))
}


Return_range_union <- function(range1,range2){
  # because we want all samples to have the sample scale on y-axis
  # we need to calculate the maximal range for y-axis
  MIN = min(range1[1],range2[1])
  MAX = max(range1[2],range2[2])
  return(c(MIN,MAX))
}


Cal_t_error_bar <- function(vector_size) {
  # https://www.r-graph-gallery.com/4-barplot-with-error-bar.html
  return(qt((1 - alpha)/2 + .5, vector_size - 1))
}


Cal_SE <- function(x){return(sd(x)/sqrt(length(x)))}


split_str <- function(x){strsplit(as.character(x),' ')}


Line_plot_TPM_geneX <- function(gene_id,condition,tpm_data,SP_anno = NA){
  # Line plot for TPM in groups
  # gene_id = Am_id
  # condition = Am_condition # for each of the biological sample
  # sample_info = Am_sample_info # for each of the group average
  # SP_anno = Am_anno
  # build data frame
  dat = data.frame(count = as.numeric(as.character(tpm_data[,gene_id])),condition = condition)
  # calculate mean and sd
  dat_mean = aggregate(dat$count, list(dat$condition), mean)
  dat_se = aggregate(dat$count, list(dat$condition), Cal_SE)
  dat_size = aggregate(dat$count, list(dat$condition), length)
  dat_t_SE = sapply(dat_size$x, Cal_t_error_bar)
  sample_info_list = unlist(sapply(dat_se$Group.1,split_str,simplify = T))
  # combine mean and se into new data frame
  dat_plot = data.frame(part = sample_info_list[seq(2,12,2)],stage = sample_info_list[seq(1,12,2)],col_mean = dat_mean$x,col_se = dat_se$x,t_SE = dat_t_SE)
  dat_plot$stage = as.numeric(revalue(dat_plot$stage,c('Early' = 1,'Intermediate' = 2,'Late' = 3)))
  # make dots not overlapping
  pd <- position_dodge(0.1) # move them .05 to the left and right
  # create the drawing object
  if ( empty(SP_anno) ) {
    title_name = paste(gene_id)
  } else {
    title_name = paste(gene_id,SP_anno[gene_id,])
  }
  p <- ggplot(dat_plot, aes(x = stage, y = col_mean, colour = part)) +
    geom_errorbar(aes(ymin = col_mean-col_se * t_SE, ymax = col_mean + col_se*t_SE),
                  width = .2, position = pd) +
    scale_x_continuous('Stage', breaks = c(1,2,3),labels = c('Early','Intermediate','Late')) +
    geom_line(position = pd,size = 2) +
    geom_point(position = pd,size = 5) +
    xlab("Stage") +
    ylab("TPM") +
    theme(axis.text = element_text(size = 20),
          axis.title = element_text(size = 15,),legend.text = element_text(size = 20)) +
    labs(title = title_name)
  return(p)
}


create_dir_if_not_exist <- function(dir) {
  # create dir only when it doesn't exist
  if ( !dir.exists(dir)) {
    dir.create(dir)
  }
}

Plot_TPM_4_Lv_id_with_unique_Am_ortholog <- function(Lv_id_list,out_sub_dir) {
  # PLOT_DIR+out_sub_dir is the final plotting directory
  # 
  # For every Lv_id in Lv_id_list, plot per gene the dot plot, line plot and one plot where the line and dot plots are combined 
  # Plot works only if it exist in Orthofinder_Sep13_Lv_all_corset_genes_ortho_Lv_trinity_Am_trinity_pairs.txt"
  # and if there are multiple Am ortholog for Lv, there will be multiple plots, in which case it is better to use Plot_TPM_4_Am_trinity_and_Am_ref
  # where all Am orthologues are in one plot with the Lv_id
  # only recommend to use when one Lv_id uniquely map to one Am_id
  # advantage: 
  #   include the trinotate annotation, as Am_trinity has annotation
  #   plot dot and line in one figure
  #
  # subst candidate
  candidate_gene_tab = all_ortho_pairs_table[which(all_ortho_pairs_table$Lv_ID %in% Lv_id_list),] # order not kept
  # > candidate_gene_tab[1,]
  #                 Lv_ID           Am_ID blast_score OP_ID     OG_ID
  # 41884 Cluster-44262.0 Cluster-48109.0         274  1692 OG0000407
  #
  # prepare output directories
  create_dir_if_not_exist(PLOT_DIR)
  out_dir = paste0(PLOT_DIR,out_sub_dir)
  create_dir_if_not_exist(out_dir)
  out_dir_lines=paste0(out_dir,'/lines/')
  out_dir_points=paste0(out_dir,'/points/')
  out_dir_combined=paste0(out_dir,'/combined/')
  out_dir_Lv_only=paste0(out_dir,'/Lv_only/')
  create_dir_if_not_exist(out_dir)
  create_dir_if_not_exist(out_dir_lines)
  create_dir_if_not_exist(out_dir_points)
  create_dir_if_not_exist(out_dir_combined)
  create_dir_if_not_exist(out_dir_Lv_only)
  # 
  N = dim(candidate_gene_tab)[1]
  for (i in seq(N)){
    Lv_id = as.character(candidate_gene_tab$Lv_ID[i])
    Am_id = as.character(candidate_gene_tab$Am_ID[i])
    blast_score = candidate_gene_tab$blast_score[i]
    op_id = candidate_gene_tab$OP_ID[i]
    og_id = candidate_gene_tab$OG_ID[i]
    p_Lv = Dot_plot_tpm_gene_X(Lv_id,Lv_tpm_data_plot,Lv_condition_plot)
    p_line_Lv = Line_plot_TPM_geneX(Lv_id,Lv_condition_plot,Lv_tpm_data_plot,Lv_anno)
    # if there is no Am ortholog
    if(Am_id == "NA" | is.na(Am_id)){ # no Am ortholog
      top_title = paste('Lv',Lv_id)
      png(paste0(out_dir_Lv_only,Lv_id,"_TPM_line_and_points_plot.png"),width = JPEG_SIZE_UNIT,height = JPEG_SIZE_UNIT*2)
      grid.arrange(p_line_Lv,p_Lv,nrow = 2,top = top_title)
      dev.off()
    } else {
      top_title = paste('Lv',Lv_id,'Am',Am_id,'BlastScore',blast_score)
      # generate plot
      p_Am = Dot_plot_tpm_gene_X(Am_id,Am_tpm_data_plot,Am_condition_plot)
      p_line_Am = Line_plot_TPM_geneX(Am_id,Am_condition_plot,Am_tpm_data_plot,Am_anno)
      # determin max for dot plot as minmal is always
      y_max = max(Am_tpm_data_plot[,Am_id],Lv_tpm_data_plot[,Lv_id])
      # determin and max and min for y-axis in line plot
      Lv_tpm_range = Return_y_max_min(p_line_Lv)
      Am_tpm_range = Return_y_max_min(p_line_Am)
      tpm_range = Return_range_union(Lv_tpm_range,Am_tpm_range)
      tpm_range[1] = min(tpm_range[1],0) # the minimal is not more than 0
      # joined/combined ones
      png(paste0(out_dir_combined,'TPM_Lv_',Lv_id,'_OP_',op_id,'_Am_',Am_id,'_blastScore_',blast_score,'_',og_id,'.png'),width = JPEG_SIZE_UNIT*2,height = JPEG_SIZE_UNIT*2)
      grid.arrange(p_line_Lv+ylim(tpm_range[1],tpm_range[2]),p_line_Am+ylim(tpm_range[1],tpm_range[2]),p_Lv+ylim(0,y_max),p_Am+ylim(0,y_max),nrow = 2,top = top_title)
      dev.off()
      # lines
      png(paste0(out_dir_lines,'Lines_TPM_Lv_',Lv_id,'_OP_',op_id,'_Am_',Am_id,'_blastScore_',blast_score,'_',og_id,'.png'),width = JPEG_SIZE_UNIT*2.7,height = JPEG_SIZE_UNIT)
      grid.arrange(p_line_Lv+ylim(tpm_range[1],tpm_range[2]),p_line_Am+ylim(tpm_range[1],tpm_range[2]),nrow = 1,top = top_title)
      dev.off()
      # dots
      png(paste0(out_dir_points,'Dots_TPM_Lv_',Lv_id,'_OP_',op_id,'_Am_',Am_id,'_blastScore_',blast_score,'_',og_id,'_.png'),width = JPEG_SIZE_UNIT*2,height = JPEG_SIZE_UNIT)
      grid.arrange(p_Lv+ylim(0,y_max),p_Am+ylim(0,y_max),nrow = 1,top = top_title)
      dev.off()
    }
  }
}


Return_ortho_info <- function(all_ortho_table,Lv_id) {
  # given Lv_id, return all Am_ids that match plus the blast_score, OG_ID, return NA if no ortholog
  # 
  # Parameters
  #   all_ortho_table is a data.frame with five columns
  #     Lv_ID, Am_ID, blast_score, OP_ID and OG_ID, e.g., 'Cluster-35395.1', 'Cluster-33808.0', 2573, 25516, 'OG0029362'
  #
  # Returns
  #   a data.frame with three columns: Am_ID, blast_score, OG_ID, such as 'Am08g33890', 435, 'OG0013845'
  #   return NA if no ortholog
  #
  # Examples:
  #   ortho_Am_trinity = Return_ortho_info(all_ortho_table_Lv_2_Am_trinity, Lv_id)
  #   ortho_Am_ref = Return_ortho_info(all_ortho_table_Lv_2_Am_ref, Lv_id)
  #
  index = which(all_ortho_table$Lv_ID == Lv_id)
  if (length(index) == 0){
    return(NA)
  } else {
    return(all_ortho_table[index, c('Am_ID','blast_score','OG_ID')])
  }
}



Plot_TPM_all_Am_4_Lv_id <- function (Lv_id,ortho_table_Lv_2_Am,Lv_tpm_data_p,Am_tpm_data_p, Lv_condition_p,Am_condition_p, Spe_Am, output_sub_dir){
  # Plot the TPM data for Lv_id and all its Am orthologues inferred by orthofinder
  # When there are multiple Am orthologus, all are plotted
  # Both average per biological group (i.e. line plot) and the raw TPM per sample (i.e. dot plot) are generated
  #
  # Parameters:
  #   Lv_id, e.g., "Cluster-28431.2"
  #   ortho_table_Lv_2_Am is a data.matrix has three columns: Am_ID, blast_score, OG_ID, such as 'Am08g33890', 435, 'OG0013845'
  #     it is usually generated by function  Return_ortho_info
  #   Lv_tpm_data_p and Am_tpm_data_p and genes are columns and samples as rows
  #   Lv_condition_p and Am_condition_p indicates the biological group for each sample in the same order as columns in *_tpm_data_p
  #   Spe_Am(str) indicate whether to get Am ortholog from Am_trinity or Am_ref 
  #   output_sub_dir tells which directory under PLOT_DIR to have the plots in
  #
  # Returns: 
  #   no returns
  #   the output goes to out_dir_lines and out_dir_points
  # 
  # Examples:
  #   Plot_TPM_all_Am_4_Lv_id (Lv_id,ortho_Am_trinity,Lv_tpm_data_plot,Am_tpm_data_plot,Lv_condition_plot, Am_condition_plot, 'Am_trinity', output_sub_dir)
  #   Plot_TPM_all_Am_4_Lv_id (Lv_id,ortho_Am_ref,Lv_tpm_data_plot,AmRef_tpm_data_plot,Lv_condition_plot, AmRef_condition_plot, 'Am_ref', output_sub_dir)
  #
  # create directories if not exist
  create_dir_if_not_exist(PLOT_DIR)
  out_dir = paste0(PLOT_DIR, output_sub_dir)
  out_dir_lines = paste0(out_dir,'/lines/')
  out_dir_points = paste0(out_dir,'/points/')
  create_dir_if_not_exist(out_dir)
  create_dir_if_not_exist(out_dir_lines)
  create_dir_if_not_exist(out_dir_points)
  # the list contains one Lv_plot and one or more Am plots depending on how many Am orthologues there are
  dot_plot_list = c()
  line_plot_list = c()
  # plot for Lv
  p_Lv = Dot_plot_tpm_gene_X(Lv_id,Lv_tpm_data_p,Lv_condition_p)
  p_line_Lv = Line_plot_TPM_geneX(Lv_id,Lv_condition_p,Lv_tpm_data_p,Lv_anno)
  # add to plot list
  dot_plot_list[[1]] = p_Lv
  line_plot_list[[1]] = p_line_Lv
  # initiate y_lim for line plot 
  Lv_tpm_range = Return_y_max_min(p_line_Lv)
  line_plot_y_lim_range = Lv_tpm_range
  # initiate y_max for dot plot 
  dot_plot_y_max = max(Lv_tpm_data_p[,Lv_id])
  # loop over all Am
  N = dim(ortho_table_Lv_2_Am)[1] # number of Am entries
  Am_id_list = c() #  sometimes there are same Lv_id and Am_id pair exist and identical, avoid plotting same twice
  plot_list_index = 2
  for (i in seq(N)){
    Am_id = ortho_table_Lv_2_Am$Am_ID[i]
    if (Am_id %in% Am_id_list){ # if plotted already, don't do anything
      next()
    }
    if (!(Am_id %in% colnames(Am_tpm_data_p))) {
      next()
    }
    Am_id_list = c(Am_id_list, Am_id)
    Am_title = paste('Am', Am_id,'BlastScore',ortho_table_Lv_2_Am$blast_score[i],ortho_table_Lv_2_Am$OG_ID[i])
    p_Am = Dot_plot_tpm_gene_X(Am_id,Am_tpm_data_p,Am_condition_p) + ggtitle(Am_title)
    p_line_Am = Line_plot_TPM_geneX(Am_id,Am_condition_p,Am_tpm_data_p,Am_anno)  + ggtitle(Am_title)
    # update max and min for y-axis in line plot
    Am_tpm_range = Return_y_max_min(p_line_Am)
    line_plot_y_lim_range = Return_range_union(line_plot_y_lim_range,Am_tpm_range)
    # update max for y-axis in dot plot
    dot_plot_y_max = max(Am_tpm_data_p[,Am_id],dot_plot_y_max)
    # add plot to the plot list
    dot_plot_list[[plot_list_index]] = p_Am
    line_plot_list[[plot_list_index]] = p_line_Am
    plot_list_index = plot_list_index +1
  }
  line_plot_y_lim_range[1] = min(0 ,line_plot_y_lim_range[1])
  # update ylim for line plots
  N_plot = length(line_plot_list)
  for (i in seq(N_plot)){
    line_plot_list[[i]] = line_plot_list[[i]] + ylim(line_plot_y_lim_range)
  }
  # update ylim for dot plots
  N_plot = length(dot_plot_list)
  for (i in seq(N_plot)){
    dot_plot_list[[i]] = dot_plot_list[[i]] + ylim(0,dot_plot_y_max)
  }
  # prepare for the plot
  plot_height = JPEG_SIZE_UNIT*ceiling(N_plot/NUM_PLOT_PER_COLUMN)
  top_title = paste('Lv',Lv_id)
  # line plot
  png(paste0(out_dir_lines,'Lines_TPM_Lv_',Lv_id,'_all_',Spe_Am,'.png'),width = JPEG_SIZE_UNIT*(NUM_PLOT_PER_COLUMN + 0.7), height =  plot_height)
  do.call("grid.arrange", c(line_plot_list, ncol = NUM_PLOT_PER_COLUMN, top = top_title))
  dev.off()
  # dot plot
  png(paste0(out_dir_points,'Dots_TPM_Lv_',Lv_id,'_all_',Spe_Am,'.png'),width = JPEG_SIZE_UNIT*NUM_PLOT_PER_COLUMN, height = plot_height)
  do.call("grid.arrange", c(dot_plot_list, ncol = NUM_PLOT_PER_COLUMN, top = top_title))
  dev.off()
}


Plot_TPM_4_Am_trinity_and_Am_ref <- function(Lv_id_list,output_sub_dir){
  # this plotting works for any Lv_id_list
  # it also returns two list of   Lv_id_list_no_Am_trinity_ortho_has_Am_ref_ortho and Lv_id_list_no_Am_trinity_ortho_no_Am_ref_ortho
  # cons: doesn't has trinotate annotation  
  create_dir_if_not_exist(PLOT_DIR)
  out_dir = paste0(PLOT_DIR, output_sub_dir)
  out_dir_lines = paste0(out_dir,'/lines/')
  out_dir_points = paste0(out_dir,'/points/')
  create_dir_if_not_exist(out_dir)
  create_dir_if_not_exist(out_dir_lines)
  create_dir_if_not_exist(out_dir_points)
  Lv_id_list_no_Am_trinity_ortho_has_Am_ref_ortho = c()
  Lv_id_list_no_Am_trinity_ortho_no_Am_ref_ortho = c()
  for (Lv_id in Lv_id_list) {
    ortho_Am_trinity = Return_ortho_info(all_ortho_table_Lv_2_Am_trinity, Lv_id)
    ortho_Am_ref = Return_ortho_info(all_ortho_table_Lv_2_Am_ref, Lv_id)
    if (!is.null(dim(ortho_Am_trinity)[1])) {
      Plot_TPM_all_Am_4_Lv_id(Lv_id,ortho_Am_trinity,Lv_tpm_data_plot,Am_tpm_data_plot,Lv_condition_plot, Am_condition_plot, 'Am_trinity', output_sub_dir)
    } else if (!is.null(dim(ortho_Am_trinity)[1])) {
      Lv_id_list_no_Am_trinity_ortho_has_Am_ref_ortho = c(Lv_id_list_no_Am_trinity_ortho_has_Am_ref_ortho, Lv_id)
    } else {
      Lv_id_list_no_Am_trinity_ortho_no_Am_ref_ortho = c(Lv_id_list_no_Am_trinity_ortho_no_Am_ref_ortho, Lv_id) 
    }
    if (!is.null(dim(ortho_Am_ref)[1])) {
      Plot_TPM_all_Am_4_Lv_id (Lv_id,ortho_Am_ref,Lv_tpm_data_plot,AmRef_tpm_data_plot,Lv_condition_plot, AmRef_condition_plot, 'Am_ref', output_sub_dir)
    }
  }
  named_list = list(Lv_id_list_no_Am_trinity_ortho_has_Am_ref_ortho = Lv_id_list_no_Am_trinity_ortho_has_Am_ref_ortho, 
                    Lv_id_list_no_Am_trinity_ortho_no_Am_ref_ortho = Lv_id_list_no_Am_trinity_ortho_no_Am_ref_ortho)
  return(named_list)
}

