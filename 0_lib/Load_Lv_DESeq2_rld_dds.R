# This R script load the rld and dds data for Lv

library(DESeq2)

WORK_DIR='~/projects/Lv_Am/'
IN_DIR = paste0( WORK_DIR,'result/R_obj/')

dds_file = paste0(IN_DIR,"Lv_dds_no_batch.rds")
rld_file = paste0(IN_DIR,"Lv_rld_no_batch.rds")

Lv_dds = readRDS(dds_file)
Lv_rld = readRDS(rld_file)

# > colnames(dds)
# [1] "Lv2_2D"   "Lv2_2V"   "Lv_2_3DX" "Lv2_3V"
# [5] "Lv2_4D"   "Lv2_4V"   "Lv2_5D"   "Lv2_5V"
# [9] "Lv4_5_2D" "Lv4_5_2V" "Lv4_5_3D" "Lv4_5_3V"
# [13] "Lv4_5_4D" "Lv4_5_4V" "Lv4_5_5D" "Lv4_5_5V"
# [17] "Lv8_9_2D" "Lv8_9_2V" "Lv8_9_3D" "Lv8_9_3V"
# [21] "Lv8_9_4D" "Lv8_9_4V" "Lv8_9_5D" "Lv8_9_5V"

Lv_sample_order_123_VD = c(18,20,22,24,17,19,21,23,10,12,14,16,9,11,13,15,2,4,6,8,1,3,5,7)

# > colnames(dds)[new_col_order]
# [1] "Lv8_9_2V" "Lv8_9_3V" "Lv8_9_4V" "Lv8_9_5V"
# [5] "Lv8_9_2D" "Lv8_9_3D" "Lv8_9_4D" "Lv8_9_5D"
# [9] "Lv4_5_2V" "Lv4_5_3V" "Lv4_5_4V" "Lv4_5_5V"
# [13] "Lv4_5_2D" "Lv4_5_3D" "Lv4_5_4D" "Lv4_5_5D"
# [17] "Lv2_2V"   "Lv2_3V"   "Lv2_4V"   "Lv2_5V"
# [21] "Lv2_2D"   "Lv_2_3DX" "Lv2_4D"   "Lv2_5D"


# > cbind(as.character(dds$condition),as.character(dds$sample_id))
# [,1]      [,2]
# [1,] "Lv2_D"   "Lv2_2D"
# [2,] "Lv2_V"   "Lv2_2V"
# [3,] "Lv2_D"   "Lv_2_3DX"
# [4,] "Lv2_V"   "Lv2_3V"
# [5,] "Lv2_D"   "Lv2_4D"
# [6,] "Lv2_V"   "Lv2_4V"
# [7,] "Lv2_D"   "Lv2_5D"
# [8,] "Lv2_V"   "Lv2_5V"
# [9,] "Lv4_5_D" "Lv4_5_2D"
# [10,] "Lv4_5_V" "Lv4_5_2V"
# [11,] "Lv4_5_D" "Lv4_5_3D"
# [12,] "Lv4_5_V" "Lv4_5_3V"
# [13,] "Lv4_5_D" "Lv4_5_4D"
# [14,] "Lv4_5_V" "Lv4_5_4V"
# [15,] "Lv4_5_D" "Lv4_5_5D"
# [16,] "Lv4_5_V" "Lv4_5_5V"
# [17,] "Lv8_9_D" "Lv8_9_2D"
# [18,] "Lv8_9_V" "Lv8_9_2V"
# [19,] "Lv8_9_D" "Lv8_9_3D"
# [20,] "Lv8_9_V" "Lv8_9_3V"
# [21,] "Lv8_9_D" "Lv8_9_4D"
# [22,] "Lv8_9_V" "Lv8_9_4V"
# [23,] "Lv8_9_D" "Lv8_9_5D"
# [24,] "Lv8_9_V" "Lv8_9_5V"
