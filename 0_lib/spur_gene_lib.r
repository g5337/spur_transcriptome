# This script provides functions for calling spur genes
# which are used in 
# 0_lib/prep_4_spur_gene_1to1_match_only_Am_ref.r
# 0_lib/prep_4_spur_gene_1to1_match_only.r:
# 5_differential_expression_spur_gene/5_4_Lv_Am_ventral_diff_exp.Rmd


###  Check whether there are statistically siginificanct difference according to
# stats_table: an obj returned by function Compare_TPM_two_conditions_gene_list

### examples after the three functions
Check_sig_diff_TPM <- function(stats_table,p_value_cutoff_local,L2FC_cutoff_local){
  # return true if there are statistically siginificanct difference in stats_table (obj returned by function Compare_TPM_two_conditions_gene_list)
  return(stats_table$p_value < p_value_cutoff_local & abs(stats_table$log2TPM_fold_change) > L2FC_cutoff_local)
}

Check_sig_up_reg_TPM <- function(stats_table,p_value_cutoff_local,L2FC_cutoff_local){
  # return true if they are statistically upregulated in stats_table  (obj returned by function Compare_TPM_two_conditions_gene_list)
  return(stats_table$p_value < p_value_cutoff_local & stats_table$log2TPM_fold_change > L2FC_cutoff_local)
}

Check_sig_down_reg_TPM <- function(stats_table,p_value_cutoff_local,L2FC_cutoff_local){
  # return true if they are statistically upregulated in stats_table  (obj returned by function Compare_TPM_two_conditions_gene_list)
  return(stats_table$p_value < p_value_cutoff_local & stats_table$log2TPM_fold_change < (-L2FC_cutoff_local))
}
# 
# # examples: 
# MIN_Log2TPM = -2 # defined not express genes as the log2(TPM) less than this cutoff; lowly expressed genes has stats NA
# stats_table_TPM_Lv_V_early_vs_Am_V_early = Compare_TPM_two_conditions_gene_list(tpm_ortho_table_log2,
#                                                                                Lv_candidate_genes,
#                                                                                Lv_V_early, 
#                                                                                Am_V_early) 
# p_value_cutoff = 0.001
# L2FC_cutoff = 2
# 
# which(Check_sig_diff_TPM(stats_table_TPM_Lv_V_early_vs_Am_V_early, p_value_cutoff, L2FC_cutoff ))
# which(Check_sig_up_reg_TPM(stats_table_TPM_Lv_V_early_vs_Am_V_early, p_value_cutoff, L2FC_cutoff))  # up in Lv
# which(Check_sig_down_reg_TPM(stats_table_TPM_Lv_V_early_vs_Am_V_early, p_value_cutoff, L2FC_cutoff)) # down in Lv, i.e., higher in Am

# > tpm_ortho_table[Lv_candidate_genes[which(Check_sig_up_reg_TPM(stats_table_TPM_Lv_V_early_vs_Am_V_early,p_value_cutoff, L2FC_cutoff))[1]],c(Lv_V_early,Am_V_early)]
#              Lv8_9_2V Lv8_9_3V Lv8_9_4V Lv8_9_5V Am8_9_2V Am8_9_3V Am8_9_4V Am8_9_5V
# Cluster-765.0 5.013614 5.842747 6.913279 9.409601 0.499555 0.647396 0.583418 0.522604

# > tpm_ortho_table[Lv_candidate_genes[which(Check_sig_down_reg_TPM(stats_table_TPM_Lv_V_early_vs_Am_V_early,p_value_cutoff, L2FC_cutoff))[1]],c(Lv_V_early,Am_V_early)]
#                Lv8_9_2V Lv8_9_3V Lv8_9_4V Lv8_9_5V Am8_9_2V Am8_9_3V Am8_9_4V Am8_9_5V
# Cluster-741.13  74.1595 63.86748 80.79372 65.42119 594.9263 606.7607 617.0502 626.9537

###  Check whether there are statistically siginificanct difference in res_local (DESeq2 result object)
### examples after the three functions

Check_sig_diff_dds <- function(res_local, padj_cutoff_local, L2FC_cutoff_local){
  # return true if there are statistically siginificanct difference in res_local (DESeq2 result object)
  return(res_local$padj < padj_cutoff_local & abs(res_local$log2FoldChange) > L2FC_cutoff_local )
}

Check_sig_up_reg_dds <- function(res_local, padj_cutoff_local, L2FC_cutoff_local){
  # return true if they are statistically upregulated in res_local (DESeq2 result object)
  return(res_local$padj < padj_cutoff_local & res_local$log2FoldChange > L2FC_cutoff_local )
}

Check_sig_down_reg_dds <- function(res_local, padj_cutoff_local, L2FC_cutoff_local){
  # return true if they are statistically upregulated in res_local (DESeq2 result object)
  return(res_local$padj < padj_cutoff_local & res_local$log2FoldChange < (-L2FC_cutoff_local) )
}
# 
# # examples:
# # calculate stats for the comparision between conditionA and conditionB in Lv (Lv_dds)
# Alpha_cutoff = 0.1 # false discovery rate
# padj_cutoff = 0.1 # should be same as Alpha_cutoff, this is adjusted p-value
# LFC_cutoff_4_pvalue = 0
# conditionA = "Early_ventral"
# conditionB = "Early_dorsal"
# res_Lv = Compare_conditions_within_species(Lv_dds, conditionA, conditionB, Alpha_cutoff, LFC_cutoff_4_pvalue)
# 
# L2FC_cutoff = 2
# which(Check_sig_diff_dds(res_Lv,padj_cutoff, L2FC_cutoff))
# which(Check_sig_up_reg_dds(res_Lv, padj_cutoff, L2FC_cutoff)) # up-regulated in V
# which(Check_sig_down_reg_dds(res_Lv, padj_cutoff, L2FC_cutoff)) # down-regulated in V, i.e. higher in D

# > assay(Lv_dds)[rownames(res_Lv[which(Check_sig_up_reg_dds(res_Lv,padj_cutoff, L2FC_cutoff))[1],]),17:24]
# Lv8_9_2D Lv8_9_2V Lv8_9_3D Lv8_9_3V Lv8_9_4D Lv8_9_4V Lv8_9_5D Lv8_9_5V 
#      19       56        9       32        9       31        9       59 

# > assay(Lv_dds)[rownames(res_Lv[which(Check_sig_down_reg_dds(res_Lv,padj_cutoff, L2FC_cutoff))[1],]),17:24]
# Lv8_9_2D Lv8_9_2V Lv8_9_3D Lv8_9_3V Lv8_9_4D Lv8_9_4V Lv8_9_5D Lv8_9_5V 
#      53        7       62        0       59        2       25        0 
# lfcSE: The standard error (SE) of log fold change is the standard deviation of its sampling distribution or an estimate of that standard deviation

# global variable the functions depends on:
# ortho_table
## > ortho_table[1,]
## Lv_corset_id    Am_corset_id
## Cluster-25859.0 Cluster-25859.0 Cluster-32219.0
# MIN_Log2TPM : a value lower than which the gene is considered as not expressed, thus no stats is calulated and NA is returned

Compare_conditions_within_species <- function(dds, conditionA, conditionB, Alpha_cutoff, LFC_cutoff) {
  # With species comparision by DESeq2
  # needs library(DESeq2)
  # parameters:
  #   Note: for a subset, change the dds to dds[candidate_gene_list,] in the input parameter
  # returns
  #   an DESeq2 result object
  # example:
  #   LFC_cutoff = 1
  #   Alpha_cutoff = 0.1 # false discovery rate cutoff
  #   conditionA = "Early_ventral"
  #   conditionB = "Early_dorsal"
  #   res_Lv = Compare_within_species(Lv_dds, conditionA, conditionB, Alpha_cutoff, LFC_cutoff)
  res = results(dds, contrast = c("condition", conditionA, conditionB), alpha = Alpha_cutoff, lfcThreshold = LFC_cutoff)
  return(res)  
}

Compare_conditions_within_species_4_candidate_genes <- function(dds, candidate_gene_list, conditionA, conditionB, Alpha_cutoff, LFC_cutoff) {
  # With species comparision by DESeq2
  # needs library(DESeq2)
  # parameters:
  #   Note: for a subset, change the dds to dds[candidate_gene_list,] in the input parameter
  # returns
  #   an DESeq2 result object
  # example:
  #   LFC_cutoff = 1
  #   Alpha_cutoff = 0.1 # false discovery rate cutoff
  #   conditionA = "Early_ventral"
  #   conditionB = "Early_dorsal"
  #   candidate_gene_list=c(NA,Lv_candidate_genes)
  #   res_Lv = Compare_within_species(Lv_dds, candidate_gene_list, conditionA, conditionB, Alpha_cutoff, LFC_cutoff)
  if ( !anyNA(candidate_gene_list)) { # there is no NA
    res = results(dds[candidate_gene_list,], contrast = c("condition", conditionA, conditionB), alpha = Alpha_cutoff, lfcThreshold = LFC_cutoff)
    return(res)
  }
  # if there is any NA
  # initiate matrix
  N = length(candidate_gene_list)
  stats_names = c("baseMean", "log2FoldChange", "lfcSE", "stat", "pvalue", "padj")
  res = matrix(NA, nrow = N, ncol = length(stats_names))
 
  # calculate res when it is not NA
  non_na_index = which(!is.na(candidate_gene_list))
  res_4_NA = results(dds[candidate_gene_list[non_na_index],], contrast = c("condition", conditionA, conditionB), alpha = Alpha_cutoff, lfcThreshold = LFC_cutoff)
  N_NA = length(non_na_index)
  for (i in seq(N_NA)) {
    res[non_na_index[i],] = unname(unlist(res_4_NA[i,]))
  }
  colnames(res) = stats_names
  res = as.data.frame(res)
  #rownames(res) = candidate_gene_list
  return(res)
}


Compare_LFC <- function(res1,res2,LFC_diff_cutoff) {
  # Check for one gene whether two LFCs are significantly different from each other 
  # Compare the LFC in res1 with that in res2 for one gene
  # Note 1: subtract the distributions of the LFC in res2 from that of res1 
  #       LFC are assume to be normal distributed according to DESeq2 method
  #       Null hypothesis: LFC_diff is not greater than the LFC_diff_cutoff
  # Note 2: pnorm parameter: lower.tail (logical) 
  #       if TRUE (default), probabilities are P[X ≤ x] otherwise, P[X > x]
  #       where in this case : x = LFC_diff_cutoff
  # Note 3: the order of res1 and res2 doesn't matter, as it returns only p.value
  # example:
  # Lv_id = "Cluster-3209.1" 
  # Am_id = ortho_table[Lv_id,]$Am_corset_id
  # res1=res_Lv[Lv_id,]
  # res2=res_Am[Am_id,]
  # Compare_LFC(res1, res2, LFC_diff_cutoff = 1)
  ## [1] 2.211573e-08
  mean_LFC_diff = abs(res1$log2FoldChange - res2$log2FoldChange)
  SE = sqrt(res1$lfcSE^2 + res2$lfcSE^2) # the standard error of the two distribution subtracted
  pvalue = pnorm(LFC_diff_cutoff, 
                 mean = mean_LFC_diff, 
                 sd = SE, 
                 lower.tail = TRUE) 
  # mean_LFC_diff = res1$log2FoldChange - res2$log2FoldChange
  #if (mean_LFC_diff > 0) {
  #  pvalue = pnorm(LFC_diff_cutoff, 
  #               mean = mean_LFC_diff, 
  #               sd = SE, 
  #               lower.tail = TRUE) # p(mean_LFC_diff<=LFC_diff_cutoff)
  #} else if (mean_LFC_diff < 0) { 
  #  pvalue = pnorm(-LFC_diff_cutoff, 
  #               mean = mean_LFC_diff, 
  #               sd = SE, 
  #               lower.tail = FALSE) # p(mean_LFC_diff<= -LFC_diff_cutoff)
  #}
  return(pvalue)
}


Compare_LFC_Lv_Am <- function(Lv_candidate_gene_list, res_Am, res_Lv, LFC_diff_cutoff) {
  # compare LFC in res_Am with that in res_Lv for the list of genes in Lv_candidate_gene_list
  # more details see function Compare_LFC
  # if there is no ortholog in Am for the Lv gene, return NA
  # the return order is the same as the input Lv_candiate_gene_list order and it is a named list
  N = length(Lv_candidate_gene_list)
  p_value_list = rep(NA,N)
  for (i in seq(N)) {
    Lv_id = Lv_candidate_gene_list[i]
    Am_id = ortho_table[Lv_id,]$Am_corset_id
    if (is.na(Am_id)) { # there is no ortholog
      p_value = NA
    } else {
      Lv_gene_res = res_Lv[Lv_id,]
      Am_gene_res = res_Am[Am_id,]
      p_value = Compare_LFC(Lv_gene_res, Am_gene_res, LFC_diff_cutoff)
    }
    p_value_list[i] = p_value
  }
  names(p_value_list) = Lv_candidate_gene_list
  return(p_value_list)
}


Estimate_conserved_diff_from_CI <- function(CI){
  # not used as it is similar to p-value estimates
  # given the confidence interval (low and high), assess the relationship of the CI of the difference in mean with 0
  # and return a conserved estimate of the difference in mean, i.e.,
  # greater than 0 (return low)
  # less than 0 (return high)
  # or overlap with 0 (return 0)
  #
  # Examples:
  # > Estimate_conserved_diff_from_CI(c(1,2))
  # [1] 1
  # > Estimate_conserved_diff_from_CI(c(-1,1))
  # [1] 0
  # > Estimate_conserved_diff_from_CI(c(-2,-1))
  # [1] -1
  low_est = CI[1]
  high_est = CI[2]
  if (is.na(low_est)) {
    return(NA)
  }
  if ((low_est < 0 && high_est > 0)) {
    return(0)
  }
  if (low_est >= 0) {
    return(low_est)
  }
  if (high_est <= 0) {
    return(high_est)
  }
  message("situation not considered",CI)
  return(NA)
}


Compare_two_CI <- function(CI_two, CI_one) {
  # return the difference between two CIs, CI_two vs CI_one
  # examples:
  # > Compare_two_CI(c(0,1),c(3,6))
  # [1] -2
  # > Compare_two_CI(c(3,6),c(0,1))
  # [1] 2
  # > Compare_two_CI(c(0,6),c(2,3))
  # [1] 0
  # 
  if (CI_two[1] > CI_one[2]) {
    return(CI_two[1] - CI_one[2])
  } 
  if (CI_two[2] < CI_one[1]) {
    return(CI_two[2] - CI_one[1])
  } 
  return(0)
}

Compare_two_CI_list <- function(CI_list_1, CI_list_2) {
  # call Compare_two_CI for a list of CIs
  # where CI_list_1 and CI_list_2 are two data.frames with two columns each
  # return a list of difference
  N1 = dim(CI_list_1)[1]
  N2 = dim(CI_list_2)[1]
  if (N1 != N2) {
    message('two CI lists have different length')
    return(NA)
  }
  CI_diff_list = rep(NA,N1)
  for (i in seq(N1)) {
    CI_1 = unlist(unname(CI_list_1[i,]))
    CI_2 = unlist(unname(CI_list_2[i,]))
    if ( !(any(is.na(CI_1)) | any(is.na(CI_2)))) {
      CI_diff_list[i] = Compare_two_CI(CI_1,CI_2)
    }
  }
  return(CI_diff_list)
}

Check_expressed <- function(x){
  # return True if the gene's expression in at least one sample is over mimal_TPM expression
  return(length(which(x > MIN_Log2TPM)) > 0)
}

Cap_at_MIN <- function(value_list){
  value_list[which(value_list < MIN_Log2TPM)]=MIN_Log2TPM
  return(value_list)
}
Compare_TPM_two_conditions_gene <- function(tpm_ortho_table_log2, candidate_gene, conditionA, conditionB) {
  # compare the tpm list from the two conditions with t.test
  # tpm_ortho_table_log2 used to be a global variable, now passed around as parameter to make the code cleaner
  # return:
  #   (from t-test)
  #   p_value: the pvalue
  #   the confidence interval of the difference in means given (log2 based)
  #   A logic indicator of whether this gene is lowly expressed
  #   Note: order to conditionA and conditionB matters, as LFC is log2(A/B)
  #
  # Example
  #   > candidate_gene="Cluster-3209.1"
  #   > conditionA=Am_V_early
  #   > conditionB=Lv_V_early
  #   > Compare_TPM_two_conditions_gene(tpm_ortho_table_log2, candidate_gene,conditionA,conditionB)
  #   [1]  0.01839125 -0.76573440 -3.04028566
  default_row_NA = c(NA,NA,NA,NA,NA) # return value when there is no ortholog in tpm_ortho_table
  default_row_low_exp = c(NA,NA,NA,NA,TRUE)
  group1_values = tpm_ortho_table_log2[candidate_gene, conditionA]
  group2_values = tpm_ortho_table_log2[candidate_gene, conditionB]
  group1_values = Cap_at_MIN(group1_values)
  group2_values = Cap_at_MIN(group2_values)
  if (anyNA(group1_values) | anyNA(group2_values)) { # if there is any NA value in  any of the two groups
    return(default_row_NA)
  }
  # filter for genes whose expression in at least one sample is over mimal_TPM expression
  if ( !Check_expressed(c(group1_values,group2_values))) {
    return(default_row_low_exp)
  }
  result = t.test(group1_values, group2_values)
  p_value = result$p.value
  log2FC = unname(result$estimate[1]) - unname(result$estimate[2])
  return(c(p_value, log2FC, result$conf.int[1], result$conf.int[2],FALSE))
}


Compare_TPM_two_conditions_gene_list <- function(tpm_ortho_table_log2, candidate_gene_list,conditionA,conditionB) {
  # return stats comparing TPM condition A with condition B over a list of candidate genes
  # for more detial see function Compare_TPM_two_conditions_gene
  # the return order is the same as the input candiate_gene_list order and the row names are the candidate gene list
  N = length(candidate_gene_list)
  stat_matrix_colnames = c("p_value",'log2TPM_fold_change',"log2TPM_fold_change_CI_low",'log2TPM_fold_change_CI_high','logic_low_exp')
  stat_matrix = matrix(NA, nrow = N, ncol = length(stat_matrix_colnames))
  colnames(stat_matrix) = stat_matrix_colnames
  for (i in seq(N)) {
    #message(candidate_gene_list[i])
    stat_matrix[i,] = Compare_TPM_two_conditions_gene(tpm_ortho_table_log2, candidate_gene_list[i],conditionA,conditionB)
  }
  rownames(stat_matrix) = candidate_gene_list
  return(as.data.frame(stat_matrix))
}


Compare_TPM_LFC_Lv_Am <- function(stats_TPM_diff_P1,stats_TPM_diff_P2,candidate_gene_list) {
  # for genes in candidate_gene_list, compare two log2FC_TPM
  # parameters:
  #   stats_TPM_diff_P1 and stats_TPM_diff_P1 are the stats for TPM comparison from function Compare_TPM_LFC_Lv_Am
  #   candidate_gene_list is a list of Lv_ids
  # returns:
  #   a data frame where the rownames are Lv_ids in candidate_gene_list
  #   two columns: 
  #     diff_FC is the difference of FC between P1 and P2
  #     conserved_est_of_diff_CI is the size of the gap between 95% CI (1-alpha from t.test default) of P1 and that of P2
  # get the confidence interval for the stats
  CI_TPM_FC_D_early = stats_TPM_diff_P1[candidate_gene_list,c('log2TPM_fold_change_CI_low','log2TPM_fold_change_CI_high')]
  CI_TPM_FC_V_early = stats_TPM_diff_P2[candidate_gene_list,c('log2TPM_fold_change_CI_low','log2TPM_fold_change_CI_high')]
  # compare the confidence interval, return the size of the gap between two intervals and 0 if overlap                  
  TPM_LFC_CI_diff_earlly_V_vs_D = Compare_two_CI_list(CI_TPM_FC_V_early, CI_TPM_FC_D_early)
  # calcualte difference in the two FC estimates
  TPM_LFC_est_diff_earlly_V_vs_D = 
    stats_TPM_diff_P2[candidate_gene_list,c('log2TPM_fold_change')] - 
    stats_TPM_diff_P1[candidate_gene_list,c('log2TPM_fold_change')]
  
  # construct data.frame for summary of stats for this step
  TPM_LFC_diff_earlly_V_vs_D = data.frame(diff_FC = TPM_LFC_est_diff_earlly_V_vs_D, 
                                           conserved_est_of_diff_CI = TPM_LFC_CI_diff_earlly_V_vs_D)
  rownames(TPM_LFC_diff_earlly_V_vs_D) = candidate_gene_list
  return(TPM_LFC_diff_earlly_V_vs_D)
}

Check_Lv_list_Am_ortholog <- function(Lv_id_list) {
  # returns two list
  # 1. Lv_id_list_no_Am_trinity_ortho_has_Am_ref_ortho
  # 2. Lv_id_list_no_Am_trinity_ortho_no_Am_ref_ortho
  Lv_id_list_no_Am_trinity_ortho_has_Am_ref_ortho = c()
  Lv_id_list_no_Am_trinity_ortho_no_Am_ref_ortho = c()
  for (Lv_id in Lv_id_list) {
    ortho_Am_trinity = Return_ortho_info(all_ortho_table_Lv_2_Am_trinity, Lv_id)
    ortho_Am_ref = Return_ortho_info(all_ortho_table_Lv_2_Am_ref, Lv_id)
    if (is.null(dim(ortho_Am_trinity)[1])) {
      if (is.null(dim(ortho_Am_ref)[1])) {
        Lv_id_list_no_Am_trinity_ortho_no_Am_ref_ortho = c(Lv_id_list_no_Am_trinity_ortho_no_Am_ref_ortho, Lv_id) 
      } else {
        Lv_id_list_no_Am_trinity_ortho_has_Am_ref_ortho = c(Lv_id_list_no_Am_trinity_ortho_has_Am_ref_ortho, Lv_id)
      }
    }
  }
  named_list = list(Lv_id_list_no_Am_trinity_ortho_has_Am_ref_ortho = Lv_id_list_no_Am_trinity_ortho_has_Am_ref_ortho, 
                    Lv_id_list_no_Am_trinity_ortho_no_Am_ref_ortho = Lv_id_list_no_Am_trinity_ortho_no_Am_ref_ortho)
  return(named_list)
}
