#!/bin/bash
# This script quantifies the transcription

# per sample
bash 2_1_1_salmon_quantification.sh

# summerizing to matrix
bash 2_1_2_salmon_matrix.sh