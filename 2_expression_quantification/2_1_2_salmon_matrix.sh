#!/bin/bash
# This script builds gene expression and transcript matrix 
# summerizing salmon output from all samples

# software
TRINITY_HOME=/applications/trinity/trinityrnaseq-Trinity-v2.8.4/

# directory
project_dir=/home/evc24/Comparative_transcriptome_spur_development/Trimmed_data


reference_fasta=${project_dir}/L_vulgaris_full/trinity/trinity/Trinity_L_vulgaris_full.fasta
fa_gene_trans_map=${project_dir}/L_vulgaris_full/trinity/trinity/Trinity.fasta.gene_trans_map

output_dir=${project_dir}/Lv_quant
quant_file=${output_dir}/quant.sf.FileList.txt

${TRINITY_HOME}/util/abundance_estimates_to_matrix.pl  --est_method salmon \
--gene_trans_map ${fa_gene_trans_map} --out_prefix salmon \
--quant_files ${quant_file} \
--name_sample_by_basedir $(quant_file)
