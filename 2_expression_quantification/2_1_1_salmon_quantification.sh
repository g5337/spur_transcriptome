#!/bin/bash
# This script quantifies the transcription per sample using salmon
# https://salmon.readthedocs.io

salmon_bin=/applications/salmon/salmon-1.0.0/bin/salmon

work_dir=/home/qw254/Lv_Am
input_dir=/home/evc24/Comparative_transcriptome_spur_development/Trimmed_data

## option 1: Lv
SP=Lv
ref_fasta=${input_dir}/L_vulgaris_full/trinity/trinity/Trinity_L_vulgaris_full.fasta
ref_index=${work_dir}/${SP}/ref_trinity/Trinity_Lv_full_salmon_index
output_dir=${work_dir}/${SP}/salmon

## option 2: Am
SP=Am
ref_fasta=${input_dir}/A_majus_full/trinity/trinity/Trinity.fasta
ref_index=${work_dir}/${SP}/ref_trinity/Trinity_Am_full_salmon_index
output_dir=${work_dir}/${SP}/salmon

## option 3: Am ref
SP=Am # used to identify input fasta file
ref_fasta=${work_dir}/Am_ref/Amajus.IGDBv3.gene.fasta
# http://bioinfo.sibs.ac.cn/Am/download_data_genome_v3/Amajus.IGDBv3.gene.fasta.gz
ref_index=${work_dir}/Am_ref/Amajus.IGDBv3.gene.fasta.salmon_index # folder
output_dir=${work_dir}/Am_ref/salmon

# make index for salmon
if [ ! -f ${ref_index}/refAccumLengths.bin ]
  then
    ${salmon_bin} index --index ${ref_index} --transcripts ${ref_fasta}
fi

# run salmon
cd $input_dir
fasta_files=`ls ${SP}*_1_trimmedv1.fastq.gz | sed 's/_1_trimmedv1.fastq.gz//g'`

for fasta_prefix in $fasta_files ; do
        r1_file=${fasta_prefix}_1_trimmedv1.fastq.gz
        r2_file=${fasta_prefix}_2_trimmedv1.fastq.gz
        if [ ! -f ${output_dir}/${fasta_prefix}_salmon.out/quant.sf ]; then
          echo "running salmon for", $fasta_prefix
          ${salmon_bin} quant --index ${ref_index} --libType IU --dumpEq --validateMappings \
            -1 ${r1_file} -2 ${r2_file} --output ${output_dir}/${fasta_prefix}_salmon.out
        fi
done
