#!/bin/bash
# This scripts cluster transcriptes to genes using corset
# https://github.com/Oshlack/Corset

corset_bin=/home/qw254/bin/corset-1.07-linux64/corset

# Option 1:
SP=Lv

# Option 2:
SP=Am

work_dir=/home/qw254/corset/
out_dir=${work_dir}/${SP}/corset
output_prefix=${out_dir}/${SP}_trinity

# parameters used for corset
# -d <double list> A comma separated list of distance thresholds. The range must be
#                 between 0 and 1. e.g -d 0.4,0.5. If more than one distance threshold
#                 is supplied, the output filenames will be of the form:
#                 counts-<threshold>.txt and clusters-<threshold>.txt
#                 Default: 0.3
# -D <double>      The value used for thresholding the log likelihood ratio. The default
#                 value will depend on the number of degrees of freedom (which is the
#                 number of groups -1). By default D = 17.5 + 2.5 * ndf, which corresponds
#                 approximately to a p-value threshold of 10^-5, when there are fewer than
#                 10 groups.
# -g <list>        Specifies the grouping. i.e. which samples belong to which experimental
#                 groups. The parameter must be a comma separated list (no spaces), with the
#                 groupings given in the same order as the bam filename. For example:
#                 -g Group1,Group1,Group2,Group2 etc. If this option is not used, each sample
#                 is treated as an independent experimental group.
# -n <string list> Specifies the sample names to be used in the header of the output count file.
#                 This should be a comma separated list without spaces.this should be a comma separated list without spaces.
#                 e.g. -n Group1-ReplicateA,Group1-ReplicateB,Group2-ReplicateA etc.
#                 Default: the input filenames will be used.
# -p <string>      Prefix for the output filenames.


DistanceThresholds='0.5'
salmon_eq_classes_files=`ls ${work_dir}/${SP}/salmon/*_salmon.out/aux_info/eq_classes.txt` # SRR*/aux_info/eq_classes.txt

output_name_list=`echo ${salmon_eq_classes_files} | sed 's/\/home\/qw254\/corset\/Lv\/salmon\///g' | sed 's/_salmon.out\/aux_info\/eq_classes.txt//g' |sed 's/ /,/g'`

#output_name_list=`echo ${salmon_eq_classes_files} | sed 's/\/home\/qw254\/corset\/Am\/salmon\///g' | sed 's/_salmon.out\/aux_info\/eq_classes.txt//g' |sed 's/ /,/g'`

# name group list
group_list=''
output_name_list_space=`echo ${output_name_list} | sed 's/,/ /g'`
for name in ${output_name_list_space};do
  echo $name
  group=${name::-2}${name:(-1)}
  group_list=${group_list}${group},
done

# remove the last comma from group_list
group_list=${group_list::-1}

${corset_bin}  -d ${DistanceThresholds} -g ${group_list} -n ${output_name_list} -p ${output_prefix} -i salmon_eq_classes ${salmon_eq_classes_files}
