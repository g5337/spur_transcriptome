#!/usr/bin/env python
# This script generate TPM and counts table for Am reference genes 

import glob, os, csv

PROJECT_DIR='/home/qw254/Lv_Am/'

## option 3: Am_ref
SP='Am_ref'
quant_file_pattern = PROJECT_DIR + SP + '/salmon/*/quant.sf'
# this order is used for the final output
sample_list = ["Am8_9_2V","Am8_9_3V","Am8_9_4V",'Am8_9_5V',
            "Am8_9_2D","Am8_9_3D","Am8_9_4D","Am8_9_5D",
            "Am4_5_2V","Am4_5_3V","Am4_5_4V","Am4_5_5V",
            "Am4_5_2D","Am4_5_3D","Am4_5_4D","Am4_5_5D",
            "Am2_2V","Am2_3V","Am2_4V","Am2_5V",
            "Am2_2D","Am2_3D","Am2_4D","Am2_5D"]

# output directory
OUT_DIR = PROJECT_DIR + SP+'/quant_table/'
fout_tpm_outfile = OUT_DIR + 'Am_ref_tpm_table.tsv'
fout_counts_outfile = OUT_DIR + 'Am_ref_count_table.tsv'

## read the tpm and count data per transcript from salmon output
d_values={}
for file in glob.glob(quant_file_pattern):
    sample_id=file.split('/')[6].replace('_salmon.out','')
    with open(file) as csvfile:
        reader = csv.DictReader(csvfile,delimiter='\t')
        for row in reader:
                gene_id=row['Name']
                tpm=row['TPM']
                count=row["NumReads"]
                if gene_id not in d_values:
                    d_values[gene_id]={}
                if not sample_id in d_values[gene_id]:
                    d_values[gene_id][sample_id]=[tpm,count]


## prepare for writing output
header='gene_id'+'\t'+'\t'.join(sample_list)+'\n'
fout_tpm =open(fout_tpm_outfile,'w')
fout_counts = open(fout_counts_outfile,'w')

## writing output
fout_tpm.write(header)
fout_counts.write(header)
for gene_id in d_values.keys():
    fout_tpm.write(gene_id)
    fout_counts.write(gene_id)
    for sample_id in sample_list:
        [tpm,count]=d_values[gene_id][sample_id]
        fout_tpm.write('\t'+tpm)
        fout_counts.write('\t'+count)
    fout_tpm.write('\n')
    fout_counts.write('\n')


fout_tpm.close()
fout_counts.close()
