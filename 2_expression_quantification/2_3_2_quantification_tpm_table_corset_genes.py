#!/usr/bin/env python
# This script makes tpm tables for corset genes for Lv and Am trinity assembly 
# Note: the counts table has been done by the corset program in 2_2_corset.sh

def Create_dict_corset_2_isoform(corset_2_isoform_file):
    dict={}
    f=open(corset_2_isoform_file)
    for l in f:
        ls=l.strip().split('\t')
        isoform_id=ls[0]
        cluster_id=ls[1]
        if not cluster_id in dict:
            dict[cluster_id]=[]
        dict[cluster_id].append(isoform_id)
    return(dict)

def Create_dict_TPM_4_isoforms(TPM_file):
    dict={}
    f=open(TPM_file)
    header=f.readline()
    for l in f:
        ls=l.strip().split('\t')
        isoform_id=ls[0]
        tpms=ls[1:]
        dict[isoform_id]=tpms
    return((dict,header))

def Sum_TPM(dict):
    L=len(next(iter(dict.values())))
    count_list=[0]*L
    for c_list in dict.values():
        for i in list(range(L)):
            count_list[i]+=float(c_list[i])
    return(count_list)


def Cal_TPM_4_corset_cluster(d_corset_2_isoform,d_TPM,cluster_id):
    if cluster_id not in d_corset_2_isoform:
        print("invalid corset_cluster_id",cluster_id)
        return(-1)
    isoforms=d_corset_2_isoform[cluster_id]
    dict={}
    for isoform_id in isoforms:
        if isoform_id not in d_TPM:
            print("invalid trinity_isoform_id",isoform_id)
        dict[isoform_id]=d_TPM[isoform_id]
    return(Sum_TPM(dict))


def Write_TPM_4_corset_cluster(d_corset_2_isoform,d_TPM,cluster_id,fout):
    count_list=Cal_TPM_4_corset_cluster(d_corset_2_isoform,d_TPM,cluster_id)
    fout.write(cluster_id)
    for c in count_list:
        fout.write('\t'+str(c))
    fout.write('\n')


INPUT_TPM_DIR = '/home/evc24/Comparative_transcriptome_spur_development/Trimmed_data/'
Lv_TPM_file = INPUT_TPM_DIR + 'Lv_quant/salmon.isoform.TPM.not_cross_norm'
Am_TPM_file = INPUT_TPM_DIR + 'Am_quant/salmon.isoform.TPM.not_cross_norm'

CORSET_DIR = '/home/qw254/corset/'
Lv_corset_2_isoform_file = CORSET_DIR + 'Lv/corset/Lv_trinity-clusters-0.5.txt'
Am_corset_2_isoform_file = CORSET_DIR + 'Am/corset/Am_trinity-clusters-0.5.txt'

d_corset_2_isoform_Lv=Create_dict_corset_2_isoform(Lv_corset_2_isoform_file)
d_corset_2_isoform_Am=Create_dict_corset_2_isoform(Am_corset_2_isoform_file)

(d_TPM_Lv,header_Lv)=Create_dict_TPM_4_isoforms(Lv_TPM_file)
(d_TPM_Am,header_Am)=Create_dict_TPM_4_isoforms(Am_TPM_file)

## Lv
fout=open('/home/qw254/corset/TPM/Lv_TPM_corset.tsv','w')
fout.write(header_Lv)
for cluster_id in d_corset_2_isoform_Lv.keys():
    Write_TPM_4_corset_cluster(d_corset_2_isoform_Lv,d_TPM_Lv,cluster_id,fout)


fout.close()


## Am
fout=open('/home/qw254/corset/TPM/Am_TPM_corset.tsv','w')
fout.write(header_Am)
for cluster_id in d_corset_2_isoform_Am.keys():
    Write_TPM_4_corset_cluster(d_corset_2_isoform_Am,d_TPM_Am,cluster_id,fout)


fout.close()
