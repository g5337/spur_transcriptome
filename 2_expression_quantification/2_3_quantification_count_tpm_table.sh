#!/bin/bash
# This script generate TPM table and counts table for corset genes

# make count table and TPM table from salmon output for Am_ref
python 2_3_1_quantification_count_tpm_table.py

# for Lv and Am trinity assembly 
# make the tpm table for corset genes 
python 2_3_2_quantification_tpm_table_corset_genes.py
