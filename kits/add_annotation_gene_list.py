#! /home/qw254/miniconda3/bin/python
# This script adds annoation to list of genes in clusters

import csv

HOME_DIR='~/projects/Lv_Am/result/'

##########
SP='Lv'
#SP='Am'
#########

WORK_DIR=HOME_DIR+'cluster_by_expression_pattern/'+SP+'/FC_1_pvalue_0.1/tables/'

cluster_file=WORK_DIR+SP+'_cluster.tsv'
output_file=WORK_DIR+SP+'_cluster_annotation_exp_stat.tsv'

#annotation_file=HOME_DIR+'trinotate_annotation/'+SP+'_corset_gene_annoation_table_corset_point5_full_version.tsv'
#SEPARATOR='\t'

annotation_file=HOME_DIR+'trinotate_annotation/'+SP+'_corset_gene_annoation_table_corset_point5_full_version.csv'
SEPARATOR=','

d_corset_id_by_cluster={}

f=open(cluster_file)
for l in f:
    ls=l.strip().split(' ')
    corset_id=ls[0]
    cluster_id=ls[1]
    d_corset_id_by_cluster[corset_id]=cluster_id

f.close()


fout=open(output_file,'w')
fout.write(SEPARATOR.join(['uniprot_id','corset_id','cluster_id','annotation'])+'\n')
with open(annotation_file) as csvfile:
    reader = csv.DictReader(csvfile,delimiter='\t')
    for row in reader:
        corset_id=row['corset_id']
        if corset_id not in d_corset_id_by_cluster:
            continue
        cluster_id=d_corset_id_by_cluster[corset_id]
        annotation=row["prot_function"]
        uniprot_id=row["swissprot_id"]
        fout.write(SEPARATOR.join([uniprot_id,corset_id,cluster_id,annotation])+'\n')

fout.close()
