# analysis_log/Lv_Am/revision_2023JanFeb/Frontier_revision_analysis_command_log.md
# This script reads two things:
# 1. spur gene list file with annotation to swiss prot genes
# 2. mapping of swiss prot genes to GO terms (~/projects/Lv_Am/result/GO_enrichment/swiss_prot_id_2_GO.tsv)
# When input a specific GO terms, it output all corset genes with its swiss prot annotation

GO_ID_X='GO:0003700'

spur_gene_file='/home/qw254/projects/Lv_Am/result/spur_gene_candidate/strategy/LF2C_1_padj_0.1_archieve_20221110/tables/summary/spur_aid_gene_summary_table.tsv'
prot_2_GO_file='/home/qw254/projects/Lv_Am/result/GO_enrichment/swiss_prot_id_2_GO.tsv'

d_prot={} # dictionary of proteins with GO term GO_ID_X

f=open(prot_2_GO_file)
header=f.readline()
for l in f:
    ls=l.split('\t')
    prot_id=ls[0]
    GOs=ls[1].split(',')
    if prot_id=='FLO_ANTMA':
        print(l)
    if GO_ID_X in GOs:
        d_prot[prot_id]=None

f.close()


f=open(spur_gene_file)
header=f.readline()
for l in f:
    ls=l.split('\t')
    corset_id=ls[0]
    anno_prot=ls[1]
    if anno_prot in d_prot.keys():
        print(corset_id, anno_prot)

f.close()
