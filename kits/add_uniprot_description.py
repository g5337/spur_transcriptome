#! /home/qw254/miniconda3/bin/python
# This script add the uniprot description and uniprot link to the spur gene list

# since the uniprot description is a huge file, it is easier to go through the file 
# line by line to retrieve the information of relavant genes.


WORK_DIR = '/home/qi/projects/Lv_Am//result/spur_gene_candidate/strategy/LF2C_1_padj_0.1/tables/'

annotatino_file = '/home/qi/projects/Lv_Am/result/uniprot_annotation/uniprot_function_all_comments.tsv' 
# this file is created by 4_annotation/4_2_1_Extract_function_from_UniprotKB_data.py on the cluster
# and transfered to desktop by scp
# scp qw254@hydrogen.plantsci.cam.ac.uk:~/projects/Lv_Am/annotation/uniprot_function_all_comments.tsv ~/projects/Lv_Am/result/uniprot_annotation/

# option 1:
tag = 'aid'

## option 2:
#tag = 'sup'

input_file = WORK_DIR+'spur_'+tag+'_gene_summary_table.tsv'
output_file = WORK_DIR+'spur_'+tag+'_gene_summary_table_with_description.tsv'

#### read input table, index the uniprot_IDs in to dictionary uniprot_dict

f = open(input_file)
l = f.readline() #  skip header

uniprot_dict={}

for l in f:
    ls = l.split('\t')
    gene_symbol=ls[1]
    if gene_symbol=='NA':
        continue
    if not gene_symbol in uniprot_dict:
        uniprot_dict[gene_symbol] = None


f.close()    

#### go through the annotation file and fill the uniprot_dict with the description

f=open(annotatino_file)
for l in f:
    if len(l)<=1: # skip empty lines
        continue
    ls=l.strip().split('\t')
    gene_symbol=ls[0]
    accession_id=ls[1]
    if len(ls)>2:
        text=ls[2]
    else:
        text='NA'
    if gene_symbol in uniprot_dict:
        uniprot_dict[gene_symbol]=[accession_id,text]


f.close()

#### go through the input table again, add the description and URL to put into output table
f = open(input_file)
l = f.readline() # header
fout = open(output_file,'w')
fout.write(l.strip()+'\tURL\tdescription\n')

for l in f:
    l_origin=l.strip()
    ls = l.split('\t')
    gene_symbol=ls[1]
    if gene_symbol[0]=='"': # if gene_symbol start with NA, it is automatically quoated e.g.,"NAT1_ARATH" 
        gene_symbol=gene_symbol[1:-1]
    if gene_symbol in uniprot_dict:
        anno = uniprot_dict[gene_symbol]
        if anno==None:
            URL='TBF' # To Be Filled
            description_text='TBF'
            print("this gene symbol", gene_symbol,"doesn't have uniprot annotation")
        else:
            [acession_id,text]=anno
            URL = 'https://www.uniprot.org/uniprot/'+acession_id
            description_text = text
    else:
        URL = 'NA'
        description_text = 'NA'
    fout.write(l_origin+'\t'+URL+'\t'+description_text+'\n')



f.close()
fout.close()

