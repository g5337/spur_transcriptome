INPUT_DIR=/home/qw254/projects/Lv_Am/result/plot/candidate_genes_plots/strategy/candidate_gene_TPM_Plots_Lv_Am

TO_DIR=/home/qw254/projects/Lv_Am/result/plot/plot_4_Apr30
#PLOT_DIR=/home/qw254/projects/Lv_Am/result/spur_gene_candidate/strategy/LF2C_1_padj_0.1/plot/individual_spur_gene_plot/

cd ${INPUT_DIR}

gene_symbol_list="IDD15 TCP8 FLO GUN8 PME41 PME31 PME8 SWT17 HEX6 BGAL3 C96AF PIN2 TAR2 CSLG3 RGP1 IX10L GATL4 CSLD1 CER1"

# loop over the GO terms

for gene_symbol in ${gene_symbol_list}
  do
  plot_file=`echo combined/*${gene_symbol}* `
  n=${#plot_file} # combined/*AAA* length 14
  if [ $n -le 16 ]
    then
    plot_file=`echo Lv_only/*${gene_symbol}* `
  fi
  if [ $n -gt 16 ]
    then
    cp ${plot_file} ${TO_DIR}
  fi
done
