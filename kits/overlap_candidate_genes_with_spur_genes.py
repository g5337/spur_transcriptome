import glob
import csv
import os


work_dir='~/projects/Lv_Am/result/Aq_candidate_genes/'
candidate_gene_Lv_file=work_dir+'candidate_gene_Lv_Am_ortho_table.txt'
candidate_gene_table=work_dir+'candidate_gene_Lv_ortho_tidy_table.txt'

spur_gene_file_dir='~/projects/Lv_Am/result/spur_gene_candidate/strategy/LF2C_1_padj_0.1/tables/'
os.chdir(spur_gene_file_dir)


d_gene_2_condition={}
for file_name in glob.glob("*.tsv"):
    file_tag='_'.join(file_name.split('_')[4:]).split('.')[0]
    f_handle=open(spur_gene_file_dir+file_name,"r")
    csv_dict = csv.DictReader(f_handle, delimiter="\t")
    Lv_cluster_header=csv_dict.fieldnames[0]
    for row in csv_dict:
        Lv_cluster_id=row[Lv_cluster_header]
        if not Lv_cluster_id in d_gene_2_condition:
            d_gene_2_condition[Lv_cluster_id]=[file_tag]
        else:
            d_gene_2_condition[Lv_cluster_id].append(file_tag)
    f_handle.close()


output_all_spur_gene_list=spur_gene_file_dir+'all_spur_gene.txt'
fout=open(output_all_spur_gene_list,'w')
for Lv_cluster_id in d_gene_2_condition:
    fout.write(Lv_cluster_id+'\n')


fout.close()

candidate_gene_Lv_ortho_dict={}
f=open(candidate_gene_Lv_file)
for l in f:
    if l[0]=='#': # skip header
        continue
    ls=l.strip().split(' ')
    candidate_gene=ls[0]
    candidate_gene_Lv_ortho_dict[candidate_gene]=[]
    for item in ls:
        if len(item)>10 and item[:10]=='Lv_Cluster':
            candidate_gene_Lv_ortho_dict[candidate_gene].append(item.split('Lv_')[1])


f.close()


fout=open(candidate_gene_table,'w')
for candidate_gene in candidate_gene_Lv_ortho_dict:
    genes=candidate_gene_Lv_ortho_dict[candidate_gene]
    for gene in genes:
        if gene in d_gene_2_condition:
            tag=';'.join(d_gene_2_condition[gene])
        else:
            tag='NA'
        fout.write(candidate_gene+'\t'+gene+'\t'+tag+'\n')


fout.close()
