---
title: "spur_gene_table"
author: "Qi"
date: "10/03/2022"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Description
This script take the spur gene tables and combine it to one single table as supplementary for the paper.

There are three tables per stage per aid/sup. Only two tables of them were included in the summary table, as a gene without Am ortholog in the reference genome are more likely to be technical artefacts. 

```{r library}
library(tidyverse)
```

```{r directory}
WORK_DIR = '~/projects/Lv_Am/'
#INPUT_DIR = paste0(WORK_DIR,'/result/spur_gene_candidate/strategy/LF2C_1_padj_0.1/tables/')
#INPUT_DIR = paste0(WORK_DIR,'/result/spur_gene_candidate/strategy/LF2C_1_padj_0.1_archieve_20220310/tables/')
INPUT_DIR = paste0(WORK_DIR,'/result/spur_gene_candidate/strategy/LF2C_1_padj_0.1_archieve_20221110/tables/')

```


```{r load table}

dat_total=c()

for(stage in c('early','intermediate','late')){
  for(aid_sup in c('aid','sup')){
    for(subset in c('genes_with_or_without_Am_ref','no_Am_trinity_ortho_has_Am_ref_ortho',"no_Am_trinity_ortho_no_Am_ref_ortho")){
      tsv_file=paste0(INPUT_DIR,'Lv_LFC1_padj_0.1_',stage,'_',aid_sup,'_', subset,'.tsv')
      dat=read_delim(tsv_file)
      dat_temp=dat[,1:8]
      colnames(dat_temp)=c("geneID","symbol","baseMean","log2FoldChange","lfcSE","stat","pvalue","padj" )
      dat_temp=dat_temp%>%add_column(stage=stage,type=aid_sup)
      dat_total=bind_rows(dat_total,dat_temp)
    }
  }
}

# > colnames(dat_total)
#  [1] "geneID"         "symbol"         "baseMean"       "log2FoldChange" "lfcSE"          "stat"          
#  [7] "pvalue"         "padj"           "stage"          "type" 

dat_total %>% select(geneID) %>% n_distinct()
# [1] 523

dat_total %>% filter(type=='aid') %>% select(geneID) %>% n_distinct()
# [1] 195
dat_total %>% filter(type=='sup') %>% select(geneID) %>% n_distinct()
# [1] 330

dat_total %>% filter(type=='sup', stage=="early") %>% select(geneID) %>% n_distinct()
#[1] 94
dat_total %>% filter(type=='sup', stage=="intermediate") %>% select(geneID) %>% n_distinct()
#[1] 197
dat_total %>% filter(type=='sup', stage=="late") %>% select(geneID) %>% n_distinct()
#[1] 134

dat_total %>% filter(type=='aid', stage=="early") %>% select(geneID) %>% n_distinct()
#[1] 61
dat_total %>% filter(type=='aid', stage=="intermediate") %>% select(geneID) %>% n_distinct()
#[1] 93
dat_total %>% filter(type=='aid', stage=="late") %>% select(geneID) %>% n_distinct()
#[1] 99

## write both aid and sup genes in one file
# output_file = paste0(INPUT_DIR,'spur_gene_summary_table.tsv')
# write_delim(dat_total,output_file,delim='\t')

# replace the comma with ; for csv files
dat_total <- dat_total %>% 
  mutate(address = str_replace(symbol, ",", ";"))

# write aid genes only
output_file = paste0(INPUT_DIR,'spur_aid_gene_summary_table.tsv')
write_delim(dat_total %>% filter(type=='aid'),output_file,delim='\t')

# write sup genes only
output_file = paste0(INPUT_DIR,'spur_sup_gene_summary_table.tsv')
write_delim(dat_total %>% filter(type=='sup'),output_file,delim='\t')

output_file = paste0(INPUT_DIR,'spur_aid_gene_summary_table.csv')
write_delim(dat_total %>% filter(type=='aid'),output_file,delim=',')

# write sup genes only
output_file = paste0(INPUT_DIR,'spur_sup_gene_summary_table.csv')
write_delim(dat_total %>% filter(type=='sup'),output_file,delim=',')


```
```{r add gene description annotation}

# detail_anno_file = paste0(WORK_DIR,'result/uniprot_annotation/uniprot_function_subset_comments.tsv')

```

