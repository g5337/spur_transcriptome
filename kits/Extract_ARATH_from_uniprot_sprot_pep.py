#! /home/qw254/miniconda3/bin/python
# This script extracts ARATH gene from swissprot full file

uniprot_DB='/home/qw254/trinotate/Am/uniprot_sprot.pep'

out_file='/home/qw254/Lv_Am/ARATH_prot/uniprot_sprot_ARATH.pep'

f=open(uniprot_DB)
fout=open(out_file,"w")
for l in f:
    if l[0]=='>':
        prot_name=l[1:].split(' ')[0]
        if prot_name.split('_')[1]=='ARATH':
            fout.write(l)
            fout.write(f.readline())


f.close()
fout.close()
