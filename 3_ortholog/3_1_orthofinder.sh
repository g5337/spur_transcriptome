#!/bin/bash
# This script runs orthofinder with 
# protein sequences by transcoder from Lv and Am trinity-assembled transcriptome
# as well as Am reference proteome

home_dir=/home/qw254/
data_dir=${home_dir}/OrthoFinder/data

## prepare data
mkdir -p ${data_dir}
cd ${data_dir}
ln -s ${home_dir}/transdecoder/Am_trinity_full_assembly/Trinity.fasta.transdecoder.pep Am_trinity_transdecoder_prot.fa
ln -s ${home_dir}/transdecoder/Trinity_L_vulgaris_full.fasta/Trinity_L_vulgaris_full.fasta.transdecoder.pep Lv_trinity_transdecoder_prot.fa
ln -s ${home_dir}/Lv_Am/Am_prot_ref/snapdragon_IGDBV1.pros.fasta Am_IGDBV1_prot.fa
# http://bioinfo.sibs.ac.cn/Am/download_data_genome_v2/02.gene_predict/snapdragon_IGDBV1.pros.fasta.gz

# the other four files downloaded to ${data_dir}
'''
Monkey Flower:
  ftp://ftp.ncbi.nih.gov/genomes/Erythranthe_guttata/protein/protein.fa.gz

Potato:
  ftp://ftp.ensemblgenomes.org/pub/plants/release-44/fasta/solanum_tuberosum/pep/Solanum_tuberosum.SolTub_3.0.pep.all.fa.gz

Tomato:
  ftp://ftp.solgenomics.net/tomato_genome/annotation/ITAG3.2_release/ITAG3.2_proteins.fasta

Arabidopsis:
  Extracted from
  ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.fasta.gz
  using "ARATH" as the species name
'''

## run OrthoFinder
orthofinder_bin=/applications/orthofinder/OrthoFinder-2.2.7/orthofinder
${orthofinder_bin} -f ${data_dir}
