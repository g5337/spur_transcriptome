#! /home/qw254/miniconda3/bin/python
# After the Orthofinder run, the sequence name are quite long
# making the visualization of trees difficult
# also the trinity assembly doesn't have corset_id making
# the interpretation difficult
# in this script, the sequence names are shortened

import os

def Add_corsetID_2_seqname(seq_name):
    # Examples:
    # >>> Add_corsetID_2_seqname('Am_trinity_transdecoder_prot_TRINITY_DN26273_c0_g2_i2.p1')
    # 'Am_TRINITY_DN26273_c0_g2_i2.p1|Cluster-0.0'
    # >>> Add_corsetID_2_seqname('ARATH_M850_ARATH')
    # 'ARATH_M850_ARATH'
    # >>> Add_corsetID_2_seqname('Am_IGDBV1_prot_Am06g04110.P01')
    # 'Am_IGDBV1_prot_Am06g04110.P01'
    # make trinity_prot_id
    temp=seq_name.split("TRINITY")
    if len(temp)>1: # it contains string 'TRINITY'
        trinity_prot_id='TRINITY'+temp[1] # trinity_prot_id set to 'TRINITY_DN26273_c0_g2_i2.p1'
    # further processing
    seq_name=seq_name.split('.p')[0] # remove .pX at the end to make it to 'TRINITY_DN26273_c0_g2_i2'
    seq_name_sections=seq_name.split('_')
    # if it is a trinity id
    if seq_name_sections[1]=='trinity':
        spe=seq_name_sections[0] # spe is species name from trinity, i.e., Lv or Am
        trinity_isoform_id='_'.join(seq_name_sections[-5:])
        # trinity_isoform_id is 'TRINITY_DN26273_c0_g2_i2.p1'
        if spe not in d_isoform_2_corset:
            print("unkown species, it needs to be either Lv or Am",spe)
        if trinity_isoform_id in d_isoform_2_corset[spe]:
            corset_id=d_isoform_2_corset[spe][trinity_isoform_id]
            return(spe+'_'+trinity_prot_id+'|'+corset_id)
        else:
            return(spe+'_'+trinity_prot_id+'|'+'Cluster-NA')
    return(seq_name) # if it is not trinity id, return the original string


def Adjust_content_of_node_label(seq_name):
    # make the seqname shorter, this is customized to the species
    #
    # >>> seq_name='Am_trinity_transdecoder_prot_TRINITY_DN30054_c0_g1_i1.p1 TRINITY_DN30054_c0_g1~~TRINITY_DN30054_c0_g1_i1.p1  ORF type_internal len_112 _-__score_19.62_Retrotrans_gag|PF03732.17|0.0077 TRINITY_DN30054_c0_g1_i1_1-333_-_:0.175367'
    # >>> Adjust_content_of_node_label(seq_name)
    # 'Am_TRINITY_DN30054_c0_g1_i1.p1|Cluster-NA'
    #
    seq_name_tag=seq_name.split(' ')[0]
    if 'TRINITY' in seq_name_tag:
        seq_name_with_corsetID=Add_corsetID_2_seqname(seq_name_tag)
        return(seq_name_with_corsetID)
    if seq_name_tag[:6]=='tomato': # Olive
        return(seq_name_tag)
    if seq_name_tag[:4]=='Olea': # Olive
        return(seq_name_tag)
    if seq_name_tag[:9]=='Acoerulea': # Olive
        return(seq_name_tag.split('Acoerulea_322_v3_1_protein_primaryTranscriptOnly_')[1])
    if seq_name_tag[:17]=='Solanum_tuberosum':
        return(seq_name_tag[18:])
        #if 'description' in seq_name:
        #    return(seq_name_tag+'|'+seq_name.split('description')[1].split('[')[0])
        #else:
        #    return(seq_name_tag)
    if seq_name_tag[:7]=='protein':
        seq_name_tag_items=seq_name_tag.split('|')
        return('MonkeyFlower_'+seq_name_tag_items[2]+'_'+seq_name_tag_items[3])
    return(seq_name)


def Make_name_short_before_colon(content):
    # before the colon, it is sequence name
    items=content.split(':') # this is ususally length two
    sub_items_0=items[0].split('(') # remove leading '('' from the string containing seqname
    L=len(sub_items_0) # this gives the number of '(' plus one
    seq_name=sub_items_0[-1] # all sub_items_0 are '', except the last one
    seq_name_adjusted=Adjust_content_of_node_label(seq_name)
    items[0]='('*(L-1)+seq_name_adjusted
    return(':'.join(items))

##########################################################
def Create_d_trinity_2_corset(infile):
    # create dictionary where d[trinity_isoform_id]=corset_id
    d={} # empty dictionary
    f=open(infile)
    for l in f:
        ls=l.strip().split('\t')
        isoform_id=ls[0]
        corset_id=ls[1]
        if isoform_id not in d:
            d[isoform_id]=corset_id
    return(d)

# create trinity id to corset id mapping for both Lv and Am trinity assembly
#corset_file_Lv='/home/qw254/corset/Lv/corset/Lv_trinity-clusters-0.5.txt'
#corset_file_Am='/home/qw254/corset/Am/corset/Am_trinity-clusters-0.5.txt'

corset_file_Lv='/home/qw254/programs/corset/Lv/corset/Lv_trinity-clusters-0.5.txt'
corset_file_Am='/home/qw254/programs/corset/Am/corset/Am_trinity-clusters-0.5.txt'


d_isoform_2_corset={}
d_isoform_2_corset['Am']=Create_d_trinity_2_corset(corset_file_Am)
d_isoform_2_corset['Lv']=Create_d_trinity_2_corset(corset_file_Lv)

##########################################################################


# set input and output directory
#WORK_DIR = '/home/qw254/OrthoFinder/data/Results_May28/WorkingDirectory/Orthologues_Feb18/'
WORK_DIR = '/home/qw254/programs/OrthoFinder/data/Results_Nov03/Orthologues_Nov05/'
INPUT_DIR = WORK_DIR + 'Recon_Gene_Trees/'
OUTPUT_DIR = WORK_DIR + 'Recon_Gene_Trees_shorter_seq_name_and_corsetID/'

# create output directory if it doesn't exist
if not os.access(OUTPUT_DIR,os.F_OK):
    os.mkdir(OUTPUT_DIR)


def Make_seqname_shorter_in_treefile(INPUT_DIR,OUTPUT_DIR,filename):
    # Examples:
    # Make_seqname_shorter_in_treefile(INPUT_DIR,OUTPUT_DIR,'OG0006573_tree.txt')
    file=INPUT_DIR+filename
    out_file=OUTPUT_DIR+filename
    f=open(file)
    fout=open(out_file,'w')
    for l in f:
        ls=l.split(',')
        item_list=[]
        for item in ls:
            item_concise=Make_name_short_before_colon(item)
            item_list.append(item_concise)
        fout.write(','.join(item_list)+'\n')
    fout.close()
    f.close()




# for all trees in the input folder
# create a tree with revised names in the output folder
file_list=os.listdir(INPUT_DIR)
for filename in file_list:
    Make_seqname_shorter_in_treefile(INPUT_DIR,OUTPUT_DIR,filename)


# The gene trees for each orthogroup and the rooted species tree are in newick format and can be viewed using FigTree (http://tree.bio.ed.ac.uk/software/figtree/).
