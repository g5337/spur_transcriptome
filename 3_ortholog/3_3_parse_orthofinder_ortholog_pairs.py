#! /home/qw254/miniconda3/bin/python

# The ortholog paris provided by orthofinder has many categories.
# This script further classify the ortholo pairs to
#  1. strict 1 to 1 match Lv to Am
#  2. multiple match but one has higher score than all else
#  3. multiple gene in Am that are likely ortholog to this Lv with same blast score
#
# Where 1 is recorded in file ```_121_strict.txt```
# 2 & 3 are recoreded in file ```*_121_topScore.txt```

# Note 1:
# Option 1 and option 2 are two different pairs

# Note 2:
# the easieset one is one to one,
# then one to many or many to one (with either equal top scores or differentiated scores)
# then many to many, which we will probably just give up

Orthofinder_run_version='Sep13'

##### option 1 #####
SpeA='Lv_trinity'
SpeB='Am_trinity'

# stats
# >>> count_strict_121
# 19950
# >>> count_topscore_one_match
# 3585
# >>> count_score_match
# 358

##### option 2 #####
SpeA='Lv_trinity'
SpeB='Am_ref'

## stats
# >>> count_strict_121
# 17890
# >>> count_topscore_one_match
# 4162
# >>> count_score_match
# 254

WORK_DIR='/home/qw254/Lv_Am/Ortholog/'

file_prefix=WORK_DIR+'Orthofinder_'+Orthofinder_run_version+'_Lv_all_corset_genes_ortho_'+SpeA+'_'+SpeB+'_pairs'

# input
ortho_file=file_prefix+'.txt'

# output
ortho_121_strict_file=file_prefix+'_121_strict.txt'
ortho_121_file=file_prefix+'_121_topScore.txt'

SCORE_MATCH=999999
## here build d where d[Lv_corset_id]=[Am_id_with_max_blastscore,max_blastscore,N, dict]
# where N is the number of corset_ids with the same max_blast score
# dict is {Am_id_1:None,Am_id_1:None,...} with all ortholog to this Lv from the input table
d={}
f=open(ortho_file)
header=f.readline() #  skip header line
# 'Lv_ID\tAm_ID\tblast_score\tOP_ID\tOG_ID\n'
for l in f:
    ls=l.strip().split('\t')
    Lv_corset_id = ls[0]
    Am_corset_id = ls[1]
    blast_score = float(ls[2])
    #OrthoPair_id = ls[3]
    #OrthoGroup_id = ls[4]
    if Lv_corset_id not in d:
        d[Lv_corset_id]=['NULL',0,0,{}]
    if Am_corset_id not in d[Lv_corset_id][3]:
        d[Lv_corset_id][3][Am_corset_id]=None
    if blast_score > d[Lv_corset_id][1]:
        d[Lv_corset_id][0]=Am_corset_id
        d[Lv_corset_id][1]=blast_score
        d[Lv_corset_id][2]=0
    elif blast_score == d[Lv_corset_id][1] and Am_corset_id != d[Lv_corset_id][0]: # the blast score is the same
        d[Lv_corset_id][2]+=1 # count the number of corset_ids with the same score

f.close()

# check how many doesn't get a match due to multiple mapping with same blast score
count_strict_121=0 # # for this Lv there is only one match in Am
count_topscore_one_match=0 # there can be multiple match but one has higher score than all else
count_score_match=0 # there are multiple gene in Am that are likely ortholog to this Lv with same blast score

for Lv_corset_id in d.keys():
    [Am_corset_id,blast_score,count,d_Am]=d[Lv_corset_id]
    if len(d_Am)==1:
        count_strict_121+=1
    elif count==0:
        # e.g. ['Am01g48470', 44.7, 0, {'Am01g48480': None, 'Am01g48470': None}]
        count_topscore_one_match+=1
    else:
        # e.g. ['Am02g30170', 154.0, 2, {'Am02g30170': None, 'Am05g31650': None}
        count_score_match+=1


count_strict_121
count_topscore_one_match
count_score_match



fout_strict=open(ortho_121_strict_file,'w')
fout=open(ortho_121_file,'w')
header='Lv_corset_id\tAm_corset_id\n'
fout.write(header)
fout_strict.write(header)
for Lv_corset_id in d.keys():
    [Am_corset_id,blast_score,count,d_Am]=d[Lv_corset_id]
    if len(d_Am)==1: # count_strict_121
        fout_strict.write(Lv_corset_id+'\t'+Am_corset_id+'\n')
    elif count==0: # count_topscore_one_match
        fout.write(Lv_corset_id+'\t'+Am_corset_id+'\n')
    else: # count_score_match
        fout.write(Lv_corset_id+'\t'+'MATCH'+'\n')


fout.close()
fout_strict.close()
