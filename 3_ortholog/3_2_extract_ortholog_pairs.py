#! /home/qw254/miniconda3/bin/python
## This script derives all possible ortholog pairs (op) from the orthofinder result
## Note: option 1 and option 2 are two runs for two different genome pairs

def Create_d_trinity_2_corset(infile):
    # create dictionary trinity id to corset_id from corset output
    # Example: d_isoform_2_corset = Create_d_trinity_2_corset(Am_trinity-clusters-0.5.txt')
    d={} # empty dictionary
    f=open(infile)
    for l in f:
        ls=l.strip().split('\t')
        isoform_id=ls[0]
        corset_id=ls[1]
        if isoform_id not in d:
            d[isoform_id]=corset_id
    return(d)


def Convert_trinity_prot_list_2_corset_list_with_counts(prot_id_list,SP):
    # map trinity prot list to corset gene list (with corset_id as values and counts of protein isoforms as values
    #
    # Parameters:
    #   prot_id_list: list of protein ids,
    #       e.g. ['TRINITY_DN40872_c0_g1_i3.p1','TRINITY_DN40872_c0_g1_i2.p1']
    #   SP(str): ''Lv_trinity' or 'Am'
    #
    # Returns:
    #    d_corset[corset_id]=N, where N is the number of isoforms with that corset_id
    #
    if SP=='Am_ref':
        return(trim_prot_2_gene_name(prot_id_list))
    d_corset={}
    for prot_id in prot_id_list:
        isoform_id=prot_id.split('.')[0]
        if isoform_id in d_isoform_2_corset[SP]:
            corset_id=d_isoform_2_corset[SP][isoform_id]
            if not corset_id in d_corset:
                d_corset[corset_id]=0
            d_corset[corset_id]+=1
    return(d_corset)


def Get_blast_score(Lv_prot_id,Am_prot_id,d_blast_score):
    # get blast score for blast Lv_prot_id to Am_prot_id
    # if there is no hits return NO_BLAST_HIT
    if not Lv_prot_id in d_blast_score:
        return(NO_BLAST_HIT)
    if not Am_prot_id in d_blast_score[Lv_prot_id]:
        return(NO_BLAST_HIT)
    return(d_blast_score[Lv_prot_id][Am_prot_id])


def Get_dict_seqID(seqID_file):
    # this part read in the blast ressult Lv_trinity to Am_trinity
    # example: d_seqID=Get_dict_seqID(seqID_file)
    #
    # parameters:
    #   seqID_file with rows like this '0_10: Am07g18490.P01'
    #
    # returns:
    #   d_seqID['0_10']='Am07g18490.P01'
    #
    dict={}
    f=open(seqID_file)
    for l in f:
        ls=l.strip().split(' ')
        seq_id=ls[0].split(':')[0]
        seq_name=ls[1] # trim from protein to isoform level
        if not seq_id in dict:
            dict[seq_id]=seq_name
    return(dict)


def Get_dict_A_blast_2_B(blast_result_file,d_seqID):
    # read in blast result where keys are seq_name instead of seq_id
    #
    # parameters:
    #   d_seqID is from function Get_dict_seqID
    #
    # returns:
    #  a dictionary with content like dict[query_id][hit_id]=bitscore
    #
    dict={}
    f=open(blast_result_file)
    for l in f:
        ls=l.split('\t')
        query_id=d_seqID[ls[0]]
        hit_id=d_seqID[ls[1]]
        bitscore=float(ls[11])
        if not query_id in dict:
            dict[query_id]={}
        if not hit_id in dict[query_id]:
            dict[query_id][hit_id]=bitscore
    return(dict)


def prot_id_2_corset_id(prot_id,SP):
    # return the corset id given trinity protein id
    isoform_id = prot_id.split('.')[0]
    if isoform_id in d_isoform_2_corset[SP]:
        return(d_isoform_2_corset[SP][isoform_id])
    else:
        return(None)


# we need a dictionary d_blast_Lv_Am[op_id][Lv_coret_id][Am_corset_id]=blast_score
# a function where the input is two list of trinity protein ids,
# and the table of blast scores between any pair of them, and the function update the _blast_Lv_Am

def Update_top_blast_score(d,Lv_pro_id_list,Am_prot_id_list):
    # read in blast result where keys are seq_name instead of seq_id
    # example:
    #   Update_top_blast_score(d,Lv_prot_id_list,Am_prot_id_list)
    #
    # parameters:
    #   d, e.g., d_blast_Lv_Am[index]
    #   d[index][Lv_corset_id][Am_gene_id] = blast_score
    #
    # returns:
    #  nothing to return, just update the input parameter d
    #
    for Lv_prot_id in Lv_pro_id_list:
        Lv_corset_id = prot_id_2_corset_id(Lv_prot_id,SpeA)
        if not Lv_corset_id in d:
            d[Lv_corset_id]={}
        for Am_prot_id in Am_prot_id_list:
            if SpeB=='Am_ref':
                Am_gene_id = Am_prot_id.split('.')[0]
            elif SpeB=='Am_trinity':
                Am_gene_id = prot_id_2_corset_id(Am_prot_id,SpeB)
            else:
                print(SpeB, 'is unknown species')
                return()
            if not Am_gene_id in d[Lv_corset_id]:
                d[Lv_corset_id][Am_gene_id]=NO_BLAST_HIT
            blast_score = Get_blast_score(Lv_prot_id,Am_prot_id,d_blast_score_SpeA_2_SpeB)
            if blast_score > d[Lv_corset_id][Am_gene_id]:
                d[Lv_corset_id][Am_gene_id] = blast_score
    return()


def trim_name(seq_list):
    # trim every string in the list to the content only before the first space
    #
    # parameters:
    # example seq in the seq_list
    # 'TRINITY_DN40872_c0_g1_i3.p1 TRINITY_DN40872_c0_g1~~TRINITY_DN40872_c0_g1_i3.p1  ORF type_internal len_169 _+__score=9.17_RVT_1|PF00078.27|4.3e-13_RT_RNaseH_2|PF17919.1|7.3e-07 TRINITY_DN40872_c0_g1_i3_1-504_+'
    #
    # returns:
    #   trimed list, where it is trimmed to 'TRINITY_DN40872_c0_g1_i3.p1'
    #
    trimmed_name_list=[]
    for seq_name in seq_list:
        trimmed_seq_name=seq_name.strip().split(' ')[0]
        trimmed_name_list.append(trimmed_seq_name)
    return(trimmed_name_list)


def trim_prot_2_gene_name(seq_list):
    # trim the list of protein ids (parameters) to isoform id list (return)
    # and remove redundancy as there can be multiple protein ids per isoform_id
    # return in a dictionary format
    d_trimmed_name={}
    for seq_name in seq_list:
        trimmed_seq_name=seq_name.strip().split('.')[0]
        if trimmed_seq_name not in d_trimmed_name:
            d_trimmed_name[trimmed_seq_name]=1
    return(d_trimmed_name)


def Read_Ortholog_file(ortho_file):
    # read the ortholog file (parameter) from speA to speB generated by orthofinder
    # return a dictinoary d['OG0000000']=[[speA_genes],[spe_Bgenes]]
    d_og={}
    f=open(ortho_file)
    header=f.readline()
    for l in f:
        ls = l.split('\t')
        og_id=ls[0]
        speA_list=trim_name(ls[1].split(','))
        speB_list=trim_name(ls[2].split(','))
        if not og_id in d_og:
            d_og[og_id]=[]
        d_og[og_id].append([speA_list,speB_list])
    f.close()
    return(d_og)


def Convert_trinity_prot_2_corset(d_og,SP_A,SP_B):
    # read in blast result where keys are trinity id or reference
    # and convert the mapping to corset id when it is trinity id
    #
    # parameters:
    #   d_og[og_id]=[[speA_list1,speB_list1],[speA_list2,speB_list2],...]
    #   SP_A(str): 'Lv_trinity' or 'Am'
    #   SP_B(str): 'Lv_trinity' or 'Am'
    #
    # returns:
    #   d_all_pairs[op_index]=[speA_corset, speB_corset]
    #       where index is numbered within this script
    #   d_op_id_2_og_id[op_index]=og_id
    #   d_blast_SpeA_SpeB[op_index][Lv_corset_id][Am_gene/corset_id] = blast_score
    index=0
    d_all_pairs={}
    d_op_id_2_og_id={}
    d_blast_SpeA_SpeB={}
    for og_id in d_og.keys():
        for [speA_list,speB_list] in d_og[og_id]:
            # speA_list=d_og['OG0000985'][0][0]
            # speB_list=d_og['OG0000985'][0][1]
            speA_corset=Convert_trinity_prot_list_2_corset_list_with_counts(speA_list,SP_A)
            speB_corset=Convert_trinity_prot_list_2_corset_list_with_counts(speB_list,SP_B)
            if len(speA_corset)>0 and len(speB_corset)>0:
                d_all_pairs[index]=[speA_corset, speB_corset]
                d_op_id_2_og_id[index]=og_id
                d_blast_SpeA_SpeB[index]={}
                Update_top_blast_score(d_blast_SpeA_SpeB[index],speA_list,speB_list)
                index+=1
    return([d_all_pairs,d_op_id_2_og_id,d_blast_SpeA_SpeB])

######### general set up

NO_BLAST_HIT = -999

dict_sample={'Am_trinity':'1', "Lv_trinity":'2', "Am_ref":'0'}
# content (not the exact string) according to
# /home/qw254/OrthoFinder/data/Results_May28/WorkingDirectory/SpeciesIDs.txt
# 0: Am_IGDBV1_prot.fa
# 1: Am_trinity_transdecoder_prot.fa
# 2: Lv_trinity_transdecoder_prot.fa
# TODO: automate the process


home_dir = '/home/qw254/'
data_dir = home_dir + "/OrthoFinder/data/Results_May28/WorkingDirectory/"

Orthofinder_run_version = 'Sep13'
work_dir = data_dir + 'Orthologues_'+Orthofinder_run_version+'/Orthologues/'

seqID_file= data_dir +'SequenceIDs.txt'


corset_dir = home_dir + 'corset/'
corset_file_Lv = corset_dir + 'Lv/corset/Lv_trinity-clusters-0.5.txt'
corset_file_Am = corset_dir + 'Am/corset/Am_trinity-clusters-0.5.txt'

d_isoform_2_corset={}
d_isoform_2_corset['Am_trinity']=Create_d_trinity_2_corset(corset_file_Am)
d_isoform_2_corset['Lv_trinity']=Create_d_trinity_2_corset(corset_file_Lv)

d_seqID=Get_dict_seqID(seqID_file)
# next(iter(d_seqID.items()))
# ('0_0', 'Am02g11900.P01')

##### start of option 1 #####
SpeA='Lv_trinity'
SpeB='Am_trinity'
## orthofinder result of Lv trinity to Am trinity
SpeA_2_SpeB_orthofinder_file = work_dir + 'Orthologues_Lv_trinity_transdecoder_prot/Lv_trinity_transdecoder_prot__v__Am_trinity_transdecoder_prot.csv'
##### end of option 1 #####

##### start of option 2 #####
SpeA='Lv_trinity'
SpeB='Am_ref'
## orthifinder result of Lv trinity to Am reference
SpeA_2_SpeB_orthofinder_file = work_dir + 'Orthologues_Lv_trinity_transdecoder_prot/Lv_trinity_transdecoder_prot__v__Am_IGDBV1_prot.csv'
##### end of option 2 #####

outfile=home_dir + 'Lv_Am/Ortholog/Orthofinder_'+Orthofinder_run_version+'_Lv_all_corset_genes_ortho_'+SpeA+'_'+SpeB+'_pairs.txt'

######### read the blast result #########

file_blast_SpeA_2_SpeB=data_dir+'Blast'+dict_sample[SpeA]+'_'+dict_sample[SpeB]+'.txt'
d_blast_score_SpeA_2_SpeB=Get_dict_A_blast_2_B(file_blast_SpeA_2_SpeB,d_seqID)

# d_blast_score_SpeA_2_SpeB[query_id][hit_id]=bitscore

######### read the orthofinder result #########
d_Lv_2_Am=Read_Ortholog_file(SpeA_2_SpeB_orthofinder_file)
# >>> d_Lv_2_Am['OG0000985'] is a list of tuple
# >>> d_Lv_2_Am['OG0000985'][0][0]
# ['TRINITY_DN11142_c0_g1_i1.p2', 'TRINITY_DN11142_c0_g1_i1.p1', 'TRINITY_DN11142_c0_g1_i3.p1', 'TRINITY_DN11142_c0_g1_i2.p1']
# >>> d_Lv_2_Am['OG0000985'][0][1]
# ['Am06g08510.P01', 'Am06g38440.P01']

######### convert trinity id to corset id in the mapping #########
[d_corset_pairs,d_op_id_2_og_id,d_blast_Lv_Am]=Convert_trinity_prot_2_corset(d_Lv_2_Am, SpeA, SpeB)

# >>> d_corset_pairs[0][0]
# {'Cluster-26996.5': 1, 'Cluster-4809.2': 1, 'Cluster-35095.0': 1, 'Cluster-49016.0': 1}
# >>> d_corset_pairs[0][1]
# dict_keys(['Am05g22360', 'Am07g10030', 'Am02g17550', 'Am03g24220', 'Am05g37680', 'Am06g05850', 'Am08g09470', 'Am04g08160', 'Am07g17650', 'Am02g51920', 'Am08g12140', 'Am01g21250'])

# >>> d_op_id_2_og_id[20423]
# 'OG0026951'

# >>> d_blast_Lv_Am[20423]
# {'Cluster-20231.0': {'Am02g18830': 118.0}}

######### make dictionary to use in writing the result #########
######### d_corset_Lv_2_op_id[Lv_corset_id]=op_id #########
# >>> next(iter(d_corset_Lv_2_op_id.items()))
# ('Cluster-26996.5', 15172)
d_corset_Lv_2_op_id={}
for op_id in d_corset_pairs.keys():
    [d1,d2]=d_corset_pairs[op_id]
    for Lv_corset_id in d1.keys():
        d_corset_Lv_2_op_id[Lv_corset_id]=op_id


### Here we write the orthologues for all Lv genes, not only the selected ones in the list ###
header='Lv_ID\tAm_ID\tblast_score\tOP_ID\tOG_ID\n'
fout=open(outfile,'w')
fout.write(header)
for Lv_corset_id in d_corset_Lv_2_op_id.keys():
    op_id=d_corset_Lv_2_op_id[Lv_corset_id]
    od_pairs=d_corset_pairs[op_id]
    print(op_id)
    og_id=d_op_id_2_og_id[op_id]
    for Lv_corset_id in od_pairs[0].keys():
        for Am_corset_id in od_pairs[1].keys():
            blast_score=d_blast_Lv_Am[op_id][Lv_corset_id][Am_corset_id]
            if blast_score>0:
                fout.write('\t'.join([Lv_corset_id,Am_corset_id,str(blast_score),str(int(op_id)),og_id])+'\n')


fout.close()


## One example
##### option 1 #####
# 25516 is op_id
# >>> d_corset_pairs[25516]
# [{'Cluster-35395.1': 1}, {'Cluster-33808.0': 1}]
# >>> d_op_id_2_og_id[25516]
# 'OG0029362'
# >>> d_blast_Lv_Am[25516]
# {'Cluster-35395.1': {'Cluster-44102.0': 2573.0}}
#
# qw254@hydrogen:~/Lv_Am/Ortholog$ grep OG0029362  /home/qw254/OrthoFinder/data/Results_May28/WorkingDirectory/Orthologues_Sep13/Orthologues/Orthologues_Lv_trinity_transdecoder_prot/Lv_trinity_transdecoder_prot__v__Am_trinity_transdecoder_prot.csv
# OG0029362	TRINITY_DN23899_c0_g1_i5.p1 TRINITY_DN35739_c0_g1_i1.p1
#
# >>> d_isoform_2_corset['Lv_trinity']['TRINITY_DN23899_c0_g1_i5']
# 'Cluster-35395.1'
# >>> d_isoform_2_corset['Am_trinity']['TRINITY_DN35739_c0_g1_i1']
# 'Cluster-33808.0'

# >>> d_blast_score_SpeA_2_SpeB['TRINITY_DN23899_c0_g1_i5.p1']['TRINITY_DN35739_c0_g1_i1.p1']
# 2573.0
