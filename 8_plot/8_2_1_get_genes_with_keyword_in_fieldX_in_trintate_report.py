#! /home/qw254/miniconda3/bin/python

import csv
import sys

def Write_result_noNA(fout,content):
    separator='\n'
    for item in content:
        if item!=None:
            fout.write(str(item)+separator)


def Build_dict_trinity_isoform_2_corset(infile):
    d={}
    f=open(infile)
    for l in f:
        ls=l.strip().split('\t')
        isoform_id=ls[0]
        corset_id=ls[1]
        d[isoform_id]=corset_id
    return d


# return the first item of fields separated by '`'
def Get_X_id(X_term):
    X_ids=[]
    for item in X_term.split('`'):
        xs=item.split('^')
        X_ids.append(xs[0])
    return X_ids


def Map_transcript_id_2_corsetID(isoform_id,SP):
    if isoform_id in d_isoform_2_corset[SP]:
        return d_isoform_2_corset[SP][isoform_id]
    else:
        return None


# read trinotate report and make dictionary
# d[GO_id]=list of trinity genes
def Trinotate_2_dict(trinotate_report,entry_name,SP):
    d={}
    f = open(trinotate_report)
    reader = csv.DictReader(f,delimiter='\t')
    for entry in reader:
        X_term=entry[entry_name]
        if X_term=='.':
            continue
        transcript_id=entry['transcript_id']
        # get the corset id from transcript/isoform ID
        gene_id=Map_transcript_id_2_corsetID(transcript_id,SP)
        # get the key for each field in the key word column
        X_ids=Get_X_id(X_term)
        for X_id in X_ids:
            if not X_id in d:
                d[X_id]=[]
            d[X_id].append(gene_id)
    return (d)


def length(x):
    return len(x)

home_dir = '/home/qw254/'
out_dir = home_dir+'enrichment/pfam/'
corset_dir = home_dir +'corset/'

trinotate_report_Lv = home_dir + 'trinotate/Lv_full/trinotate_annotation_Lv_full_report.xls'

trinotate_report_Am = home_dir + 'trinotate/Am/trinotate_annotation_Am_report.xls'

corset_file_Lv = corset_dir + 'Lv/corset/Lv_trinity-clusters-0.5.txt'
corset_file_Am = corset_dir + 'Am/corset/Am_trinity-clusters-0.5.txt'

key_id_list=['PF00134.23','PF02984.19']

d_isoform_2_corset={}
d_isoform_2_corset['Am']=Build_dict_trinity_isoform_2_corset(corset_file_Am)
d_isoform_2_corset['Lv']=Build_dict_trinity_isoform_2_corset(corset_file_Lv)

d_gene_key={}
d_gene_key['Am']=Trinotate_2_dict(trinotate_report_Am,'Pfam','Am')
d_gene_key['Lv']=Trinotate_2_dict(trinotate_report_Lv,'Pfam','Lv')

for key_id in key_id_list:
    print("creating output for "+key_id)
    for SP in ['Am','Lv']:
        if key_id in d_gene_key[SP]:
            fout=open(out_dir+'/'+key_id+'_'+SP+'.txt','w')
            Write_result_noNA(fout,set(d_gene_key[SP][key_id]))
            fout.write('\n')
            fout.close()
        else:
            print('key term',key_id, "doesn't exit for", SP)
