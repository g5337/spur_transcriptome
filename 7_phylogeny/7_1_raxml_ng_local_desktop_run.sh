#!/bin/bash
# This script builds the phylogenetic tree for gene groups
# using raxml_ng

raxml_ng_BIN=~/bin/raxml-ng/raxml-ng

# data preparation
# Seq_Matt_Box_tree_Lb_Lc_seq_added_STM_added.txt downloaded from Erin's email on 05 Feb 2020
data_dir=/data/ownCloud/Qi/Project/Lv_Am/phylogenetic_reconstruction/Erin_seq_4_trees/
cd ${data_dir}
ln -s Seq_Matt_Box_tree_Lb_Lc_seq_added_STM_added.txt Seq_Matt_Box_tree_Lb_Lc_seq_added_STM_added.fa

# set parameters
tag=Seq_Matt_Box_tree_Lb_Lc_seq_added_STM_added

# set input and output files
aa_fasta_file=${data_dir}/${tag}.fa
msa_file=${data_dir}/${tag}.msa

# step 1:
# run
mafft --auto ${aa_fasta_file} >${msa_file}
${raxml_ng_BIN} --all --msa ${msa_file} --model LG+G8+F --tree pars{10} --bs-trees 200

# step 2:
# summerize bootstrap tree
${raxml_ng_BIN} --support --tree ${msa_file}.raxml.bestTree  --bs-trees ${msa_file}.raxml.bootstraps
