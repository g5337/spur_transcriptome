#!/bin/bash
# This script copies the existing candidate genes profile to the folder
# of each GO term, in order to help inverstigate the expression profile
# for spur genes for each GO term,

GO_pvalue=0.01 # cutoff used for GO enrichemnt analysis

WORK_DIR=/home/qw254/projects/Lv_Am/result/spur_gene_candidate/strategy/LF2C_1_padj_0.1

INPUT_DIR=${WORK_DIR}/GO_enrichment_all/BP/pvalue_cutoff_${GO_pvalue}_node_size_cutoff_5/list_of_spur_gene_per_GO_term
PLOT_DIR=${WORK_DIR}/plot/individual_spur_gene_plot/

cd ${INPUT_DIR}
# loop over the GO terms
file_list=`ls *.tsv`
for file_name in ${file_list}
  do
    GO_id=`echo $file_name | cut -f1 -d'.' | cut -f4 -d'_'`
    # make folder for the GO term
    folder_4_current_GO_id=corset_gene_plots_4_${GO_id}_folder
    mkdir ${folder_4_current_GO_id}
    # loop over all corset cluster_id in the file
    corset_id_list=`cat ${file_name}`
    for corset_id in ${corset_id_list}
      do
      # check whether it exist in Lv_only folder
      plot_file=`ls ${PLOT_DIR} | grep ${corset_id}`
      plot_file=${PLOT_DIR}${plot_file}
      if [[ ! -z ${plot_file} ]]
        then
          cp ${plot_file} ${folder_4_current_GO_id}
      else
        echo "candidate gene expression plot doesn't exist"${corset_id}
      fi
    done
done

'''
PLOT_DIR=/home/qw254/projects/Lv_Am/result/plot/candidate_genes_plots/strategy/candidate_gene_TPM_Plots_Lv_Am

Lv_only_DIR=${PLOT_DIR}/Lv_only/
Lv_ortho_Am_DIR=${PLOT_DIR}/combined/

# loop over the GO terms
file_list=`ls *.tsv`
for file_name in ${file_list}
  do
    GO_id=`echo $file_name | cut -f1 -d'.' | cut -f4 -d'_'`
    # make folder for the GO term
    folder_4_current_GO_id=corset_gene_plots_4_${GO_id}_folder
    mkdir ${folder_4_current_GO_id}
    # loop over all corset cluster_id in the file
    corset_id_list=`cat ${file_name}`
    for corset_id in ${corset_id_list}
      do
      # check whether it exist in Lv_only folder
      plot_file=`ls ${Lv_only_DIR} | grep ${corset_id}`
      plot_file=${Lv_only_DIR}${plot_file}
      if [[ ! -z ${plot_file} ]]
        then
          cp ${plot_file} ${folder_4_current_GO_id}
      else
          # not, then check the combined folder
          plot_file=`ls ${Lv_ortho_Am_DIR}| grep ${corset_id} | grep -v Am_${corset_id} `
          plot_file=${Lv_ortho_Am_DIR}${plot_file}
          if [[ ! -z ${plot_file} ]]
          then
            # copy the expression plot to the current_GO_id_fold
            cp ${plot_file} ${folder_4_current_GO_id}
          else
            echo "${corset_id} plot not found"
        fi
      fi
      done
done
'''
