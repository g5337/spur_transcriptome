---
title: "Am PCA"
author: "Qi"
date: "28/10/2020"
output: html_document
---


```{r libraries, message = F}
library(DESeq2)
library(ggplot2)
library(ggrepel)

home_dir = '~/'
lib_dir = paste0(home_dir,'gitlab/spur_transcriptome/0_lib/') 
source(paste0(lib_dir,'rename_replicate_samples.R'))

```

```{r load data}
# output setting
SP='Am'

WORK_DIR = '~/projects/Lv_Am/'
IN_DIR = paste0( WORK_DIR,'result/R_obj/')

#vsd_file = paste0(IN_DIR,"Am_vsd_no_batch.rds")
#out_dir=paste0(WORK_DIR,'result/PCA/')


vsd_file = paste0(IN_DIR,"Am_vsd_no_batch_outlier_removed.rds")
out_dir=paste0(WORK_DIR,'result/PCA/outlier_removed/')

output_tag_base=paste0(out_dir,SP,'_PCAplot_')
vsd = readRDS(vsd_file)
```

```{r PCA function}

plotPCA_withmorePC <- function(object, intgroup = "condition", ntop = 500, returnData = FALSE) {
    rv <- rowVars(assay(object))
    select <- order(rv, decreasing = TRUE)[seq_len(min(ntop, 
        length(rv)))]
    pca <- prcomp(t(assay(object)[select, ]))
    percentVar <- pca$sdev^2/sum(pca$sdev^2)
    if (!all(intgroup %in% names(colData(object)))) {
        stop("the argument 'intgroup' should specify columns of colData(dds)")
    }
    intgroup.df <- as.data.frame(colData(object)[, intgroup, 
        drop = FALSE])
    group <- if (length(intgroup) > 1) {
        factor(apply(intgroup.df, 1, paste, collapse = " : "))
    }
    else {
        colData(object)[[intgroup]]
    }
    d <- data.frame(PC1 = pca$x[, 1], PC2 = pca$x[, 2], PC3 = pca$x[, 3], PC4 = pca$x[, 4], PC5 = pca$x[, 5],PC6 = pca$x[, 6],PC7 = pca$x[, 7], group = group, 
        intgroup.df, name = colnames(object))
    if (returnData) {
        attr(d, "percentVar") <- percentVar
        return(d)
    }
}
```

```{r PCA setup}

individual = vsd$replicate

# replace the replicate names
individual=Rename_replicate_rA(individual)
vsd$replicate=individual

#individual_shape = c('2' = 15, '3' = 16, '4' = 17, '5' = 18)
individual_shape = c('A' = 15, 'B' = 16, 'C' = 17, 'D' = 18)

condition_color = c("Late_ventral" = "#66C2A5","Late_dorsal" = "#FC8D62","Intermediate_ventral" = "#8DA0CB","Intermediate_dorsal" = "#E78AC3","Early_ventral" = "#A6D854","Early_dorsal" = "#FFD92F")

shapeScale <- scale_shape_manual(name = "individual",values = individual_shape)
colScale <- scale_colour_manual(name = "condition",values = condition_color)

```

# PCA first 500 genes

```{r PCA from first 500 genes}
pcaData <- plotPCA(vsd, intgroup = c("condition"), returnData = TRUE)
pcaData$individual = individual
```

```{r get PCA data}
pcaData <- plotPCA_withmorePC(vsd, intgroup = c("condition"), returnData = TRUE)
output_tag=paste0(output_tag_base,'top500_genes_')
pcaData$individual = vsd$replicate
```

``` {r PCA percentage}
percentVar <- round(100 * attr(pcaData, "percentVar"))
barplot(percentVar)
```

``` {r PCA 1 and 2}
p = ggplot(pcaData, aes(PC1, PC2, color = condition, shape = individual)) +
  geom_point(size = 3,alpha = 0.8) +
  xlab(paste0("PC1: ",percentVar[1],"% variance")) +
  ylab(paste0("PC2: ",percentVar[2],"% variance")) + 
  coord_fixed() + colScale + shapeScale

tag='PC1_PC2'
output_file=paste0(output_tag,tag,'.pdf')
pdf(output_file,width=7,height=4)
p
dev.off()

```

``` {r PCA 3 and 4}
p34 = ggplot(pcaData, aes(PC3, PC4, color=condition,shape=individual)) +
  geom_point(size=3,alpha = 0.8) +
  xlab(paste0("PC3: ",percentVar[3],"% variance")) +
  ylab(paste0("PC4: ",percentVar[4],"% variance")) + 
  coord_fixed() + colScale + shapeScale

tag='PC3_PC4'
output_file=paste0(output_tag,tag,'.pdf')
pdf(output_file,width=7,height=4)
p34
dev.off()

```

Am2_4D is an ourlier, looks more like ventral sample than dorsal sample from the same stage


``` {r pc 56}

p56=ggplot(pcaData, aes(PC5, PC6, color=condition,shape=individual)) +
  geom_point(size=3,alpha = 0.8) +
  xlab(paste0("PC5: ",percentVar[5],"% variance")) +
  ylab(paste0("PC6: ",percentVar[6],"% variance")) + 
  coord_fixed() + colScale + shapeScale



tag='PC5_PC6'
output_file=paste0(output_tag,tag,'.pdf')
pdf(output_file,width=7,height=4)
p56
dev.off()

```

# PCA with all genes

```  {r all genes PCA data obj}
pcaData <- plotPCA_withmorePC(vsd, intgroup=c("condition"), returnData=TRUE,ntop=dim(vsd)[1])
output_tag=paste0(output_tag_base,'all_genes_')
pcaData$individual=vsd$replicate
```

``` {r all genes PCA percentage}
percentVar <- round(100 * attr(pcaData, "percentVar"))
barplot(percentVar)
```

``` {r all genes PCA 1 and 2}
p = ggplot(pcaData, aes(PC1, PC2, color = condition, shape = individual)) +
  geom_point(size = 3,alpha = 0.8) +
  xlab(paste0("PC1: ",percentVar[1],"% variance")) +
  ylab(paste0("PC2: ",percentVar[2],"% variance")) + 
  coord_fixed() + colScale + shapeScale

tag='PC1_PC2'
output_file=paste0(output_tag,tag,'.pdf')
pdf(output_file,width=7,height=4)
p
dev.off()

```

``` {r all genes PCA 3 and 4 }
p34=ggplot(pcaData, aes(PC3, PC4, color=condition,shape=individual)) +
  geom_point(size=3,alpha = 0.8) +
  xlab(paste0("PC3: ",percentVar[3],"% variance")) +
  ylab(paste0("PC4: ",percentVar[4],"% variance")) + 
  coord_fixed() + colScale + shapeScale


tag='PC3_PC4'
output_file=paste0(output_tag,tag,'.pdf')
pdf(output_file,width=7,height=4)
p34
dev.off()
```

``` {r all genes PCA 5 and 6}

p56=ggplot(pcaData, aes(PC5, PC6, color=condition,shape=individual)) +
  geom_point(size=3,alpha = 0.8) +
  xlab(paste0("PC5: ",percentVar[5],"% variance")) +
  ylab(paste0("PC6: ",percentVar[6],"% variance")) + 
  coord_fixed() + colScale + shapeScale


tag='PC5_PC6'
output_file=paste0(output_tag,tag,'.pdf')
pdf(output_file,width=7,height=4)
p56
dev.off()

```
