#!/bin/bash
# This script performs QC of all sequencing reads
# https://www.bioinformatics.babraham.ac.uk/projects/fastqc/

PROJECT_DIR=/home/evc24/Comparative_transcriptome_spur_development/

fastqc_bin=/applications/fastqc/fastqc_v0.11.5/fastqc

input_dir=${PROJECT_DIR}/Trimmed_data
output_dir=${input_dir}/fastqc

fastqfile_suffix=fastq.gz

N_cpu=8

for file in ${input_dir}/*.${fastqfile_suffix}
do
  echo "now processing" $file
  ${fastqc_bin} -t ${N_cpu} -o ${output_dir} $file
done

# conda install multiqc
multiqc ${output_dir}
