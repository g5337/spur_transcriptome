#!/bin/bash
# This script calculates ExN50 as part of assembled transcriptome QC
# https://github.com/trinityrnaseq/trinityrnaseq/wiki/Transcriptome-Contig-Nx-and-ExN50-stats

# ExN50 calculation
PROJECT_DIR=/home/qw254/projects/Lv_Am/evc24/projects/Lv_Am_spur_transcriptome/analysis/
work_dir=${PROJECT_DIR}/trinity_assembly

## option 1: Lv
reference_fasta=${work_dir}/L_vulgaris_full/trinity/trinity/Trinity_L_vulgaris_full.fasta
quant_dir=${PROJECT_DIR}/Lv_quant
output_prefix=${PROJECT_DIR}/QC/Lv

## option 2: Am
reference_fasta=${work_dir}/A_majus_full/trinity/trinity/Trinity.fasta
quant_dir=${PROJECT_DIR}/Am_quant
output_prefix=${PROJECT_DIR}/QC/Am


TRINITY_HOME=/applications/trinity/trinityrnaseq-Trinity-v2.8.4

${TRINITY_HOME}/util/misc/contig_ExN50_statistic.pl \
  ${quant_dir}/salmon.isoform.counts.matrix ${reference_fasta} | \
  tee ${output_prefix}_ExN50.stats
