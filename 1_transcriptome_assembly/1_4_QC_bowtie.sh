#!/bin/bash
# This script aligns all reads to the assembled transcriptome as part of QC
# https://github.com/trinityrnaseq/trinityrnaseq/wiki/RNA-Seq-Read-Representation-by-Trinity-Assembly
# http://bowtie-bio.sourceforge.net/bowtie2/index.shtml

PROJECT_DIR=/home/evc24/Comparative_transcriptome_spur_development/
WORK_DIR=${PROJECT_DIR}/Trimmed_data

## option 1: Lv
assembly_id=Lv_full
bowtie_index_fasta=${WORK_DIR}/L_vulgaris_full/trinity/trinity/Trinity_L_vulgaris_full.fasta
sample_list='Lv2_2V Lv2_2V Lv2_2D   Lv2_2D Lv4_5_2V Lv4_5_2V
                     Lv4_5_2D Lv4_5_2D Lv8_9_2V Lv8_9_2V Lv8_9_2D Lv8_9_2D
                     Lv2_3V Lv2_3V Lv_2_3DX Lv_2_3DX Lv4_5_3V Lv4_5_3V
                     Lv4_5_3D Lv4_5_3D Lv8_9_3V Lv8_9_3V Lv8_9_3D Lv8_9_3D
                     Lv2_4V Lv2_4V Lv2_4D Lv2_4D Lv4_5_4V Lv4_5_4V Lv4_5_4D Lv4_5_4D
                     Lv8_9_4V Lv8_9_4V Lv8_9_4D Lv8_9_4D Lv2_5V Lv2_5V
                     Lv2_5V Lv2_5D Lv4_5_5V Lv4_5_5V Lv4_5_5D Lv4_5_5D
                     Lv8_9_5V Lv8_9_5V Lv8_9_5D Lv8_9_5D'

## option 1: Am
assembly_id=Am_full
bowtie_index_fasta=${WORK_DIR}/A_majus_full/trinity/trinity/Trinity.fasta
sample_list='Am2_2V Am2_2V Am2_2D Am2_2D Am4_5_2V Am4_5_2V Am4_5_2D Am4_5_2D
                Am8_9_2V Am8_9_2V Am8_9_2D Am8_9_2D Am2_3V Am2_3V_2 Am2_3V Am2_3D
                Am4_5_3V Am4_5_3V Am4_5_3D Am4_5_3D Am8_9_3V Am8_9_3V Am8_9_3D Am8_9_3D
                Am2_4V Am2_4V_2 Am2_4D Am2_4D Am4_5_4V Am4_5_4V Am4_5_4D Am4_5_4D
                Am8_9_4V Am8_9_4V Am8_9_4D Am8_9_4D Am2_5V Am2_5V Am2_5V Am2_5D
                Am4_5_5V Am4_5_5V Am4_5_5D Am4_5_5D Am8_9_5V Am8_9_5V Am8_9_5D Am8_9_5D'


# set software locations
bowtie2_dir=/applications/bowtie2/bowtie2-2.3.4.2
samtools_bin=/applications/samtools/samtools-1.9/samtools

#MEM=20G # this is not used in this script, just a note for cluster submission
N_CPU=12 # for both bowtie and samtools

# tags in the fastq.gz files to distinguish read1 file from read2 file
R1_tag='1'
R2_tag='2'

# set the input and output directories
input_dir=${WORK_DIR}
output_dir=${WORK_DIR}/bowtie_data

# the assmebly to align to
bowtie_index=${output_dir}/indexed_file_${assembly_id}_Trinity_fasta # output index

#create the output directory if it is not already there
if [ ! -d ${output_dir} ]
  then
    mkdir ${output_dir}
fi

set -x # use the debug mode of shell to see all the variables

# build a bowtie2 index for the transcriptome.
if [ ! -f ${bowtie_index}.1.bt2 ]; then
    echo "building index for ", ${assembly_id}
    ${bowtie2_dir}/bowtie2-build  ${bowtie_index_fasta} ${bowtie_index}
   fi

#looping through the files to perform bowtie
for sample_id in ${sample_list}
        do
        echo "processing sample", $sample_id
        #prepare the input fastq files for alignment
        fastq_r1=${WORK_DIR}/${sample_id}_${R1_tag}_trimmedv1.fastq.gz
        fastq_r2=${WORK_DIR}/${sample_id}_${R2_tag}_trimmedv1.fastq.gz
        # align reads to bowtie indexed file.
        # ? If I use N_CPU for align, another N for samtools, does it mean I need total 2*N_CPU ?
        # prepare output file names
        stat_file=${output_dir}/align_stats_${assembly_id}_${sample_id}_Trinity_fasta.txt
        output_bam_file=${output_dir}/align_stats_${assembly_id}_${sample_id}_Trinity_fasta.bam
        # used bowtie parameters
        #  -k <int>          report up to <int> alns per read; MAPQ not meaningful
        # -q                 query input files are FASTQ .fq/.fastq (default)
        # --no-unal Suppress SAM records for reads that failed to align.
        ${bowtie2_dir}/bowtie2 -p ${N_CPU} -q --no-unal -k 20 \
         -x ${bowtie_index}  -1 ${fastq_r1} -2 ${fastq_r2} \
         2> ${stat_file}| \
         ${samtools_bin} view -@${N_CPU} -Sb \
         -o ${output_bam_file}
         cat 2>&1 ${stat_file} # write the content of the stat file standard output
     done

set +x # undo set -x

# Get the alignment stats for bowtie and show in standard output
cd ${output_dir}

files=`ls *txt`

for file in $files; do
  echo $file
  tail -1 $file
done
