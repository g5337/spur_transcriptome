#!/bin/bash
# This script uses cutadapt (https://cutadapt.readthedocs.io/en/stable/) 
# to trim unwanted segments from the reads 

PROJECT_DIR=/home/evc24/Comparative_transcriptome_spur_development/

CUTADAPT_BIN=/applications/cutadapt/cutadapt-1.3/bin/cutadapt

ADAPTER_FWD=AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT
ADAPTER_REV=GATCGGAAGAGCACACGTCTGAACTCCAGTCACATCACGATCTCGTATGCCGTCTTCTGCTTG

INPUT_DIR=${PROJECT_DIR}/raw_data
OUTPUT_DIR=${PROJECT_DIR}/Trimmed_data

SAMPLE_ID=Lv4_5_2D # example

${CUTADAPT_BIN} -a ${ADAPTER_FWD} -A ${ADAPTER_REV} \
        -o ${OUTPUT_DIR}/${SAMPLE_ID}_1_trimmedv1.fastq.gz \
        -p ${OUTPUT_DIR}/${SAMPLE_ID}_2_trimmedv1.fastq.gz \
        ${INPUT_DIR}/${SAMPLE_ID}_1.fq.gz \
        ${INPUT_DIR}/${SAMPLE_ID}_2.fq.gz
