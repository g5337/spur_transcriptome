#!/bin/bash
# This script performs transcriptome assembly with trinity
# https://github.com/trinityrnaseq/trinityrnaseq

## option 1 : for Lv
assembly_name=L_vulgaris_full
sample_file=${input_dir}/Lv_samples.txt

## option 2 : for Am
assembly_name=A_majus_full
sample_file=${input_dir}/Am_samples.txt

## set folders for both option 1 and option 2
PROJECT_DIR=/home/evc24/Comparative_transcriptome_spur_development/
input_dir=${PROJECT_DIR}/Trimmed_data/
output_dir=${input_dir}/${assembly_name}/trinity
temp_output_dir=/local_data/erin_linaria/${assembly_name}/trinity

# trinity software package location
trinity_bin=/applications/trinity/trinityrnaseq-Trinity-v2.8.4/Trinity
# parameters to run trinity
MEM=240G
N_CPU=24

mkdir -p ${temp_output_dir}
cd ${input_dir}

# run and put the output to ${temp_output_dir}
${trinity_bin} --seqType fq --max_memory ${MEM} --samples_file ${sample_file} \
  --CPU ${N_CPU} --output ${temp_output_dir}

check=$? # $? gives the exit code for the last command, i.e., trinity run
if [ "$check" -eq 0 ]; then
  echo "trinity run for",${assembly_name},"successful!"
  # remove two bigger files
  rm ${temp_output_dir}/both.fa
  rm ${temp_output_dir}/scaffolding_entries.sam
else
  echo "trinity run for",${assembly_name},"failed..."
fi

# it trinity run finishes properly, copy content from temp folder to output folder 
cp -r ${temp_output_dir} ${output_dir}
