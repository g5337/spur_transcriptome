#!/bin/bash
# This scripts evaluate the completeness of the assembled transcriptome
# by blasting it against Am proteome

PROJECT_DIR=/home/evc24/Comparative_t
work_dir=${PROJECT_DIR}/Trimmed_data/

Am_prot_DB=/home/qw254/Lv_Am/Am_prot_ref/snapdragon_IGDBV1.pros.fasta
# http://bioinfo.sibs.ac.cn/Am/download_data_genome_v2/02.gene_predict/snapdragon_IGDBV1.pros.fasta.gz

trinity_home=/applications/trinity/trinityrnaseq-Trinity-v2.8.4

## option 1: Lv
query_nt_transcripts=${work_dir}/L_vulgaris_full/trinity/trinity/Trinity_L_vulgaris_full.fasta
output_dir=${work_dir}/Lv_protein/

## option 2: Am
query_nt_transcripts=${work_dir}/A_majus_full/trinity/trinity/Trinity.fasta
output_dir=${work_dir}/Am_protein/

## general
blastx_outfile=${output_dir}/blastx.outfmt6
stats_file=${output_dir}/blast_stats.txt

## prepare for blast
makeblastdb -in ${Am_prot_DB} -dbtype prot

## Perform the blast search, reporting only the top alignment:
if [ ! -f ${blastx_outfile} ]; then
  blastx -query ${query_nt_transcripts} -db ${Am_prot_DB} -out ${blastx_outfile} \
        -evalue 1e-20 -num_threads 6 -max_target_seqs 1 -outfmt 6
fi

## gather stats
${trinity_home}/util/analyze_blastPlus_topHit_coverage.pl ${blastx_outfile}\
  ${query_nt_transcripts} ${Am_prot_DB} > ${stats_file}
