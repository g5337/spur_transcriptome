#!/bin/bash
# This scripts runs two scripts which predicts the peptides 
# for the assembled transcripts for Lv (option 1) and Am (option 2)
# It includes homology searches as ORF retention criteria.

PROJECT_DIR=/home/evc24/Comparative_transcriptome_spur_development/

# start of options 1 & 2

#option 1: Lv
input_fasta_file=${PROJECT_DIR}/Trimmed_data/L_vulgaris_full/trinity/trinity/Trinity_L_vulgaris_full.fasta
fasta_basename=`basename ${input_fasta_file}`
working_dir=${PROJECT_DIR}/transdecoder/${fasta_basename} # output dir

#option 2: Am
input_fasta_file=${PROJECT_DIR}/Trimmed_data/A_majus_full/trinity/trinity/Trinity.fasta
working_dir=/home/qw254/transdecoder/Am_trinity_full_assembly/

# end of options 1 & 2

N_CPU=24 # For this script on the cluster, 4G memory and 24 CPUs were used 

transdecoder_dir=/applications/transdecoder/TransDecoder-v5.5.0
# https://github.com/TransDecoder/TransDecoder/wiki

hmmscan_bin=/usr/bin/hmmscan
# >hmmscan -h
# hmmscan :: search sequence(s) against a profile database
# HMMER 3.2.1 (June 2018); http://hmmer.org/

BLASTP_DB=/data/public_data/uniport/uniref90/uniref90.fasta
# Downloaded 2019 Jan 31 from https://ftp.uniprot.org/pub/databases/uniprot/uniref/uniref90/uniref90.fasta.gz

PFAM_DB=/data/public_data/Pfam/30_08_2018/Pfam-A.hmm
# http://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam32.0/Pfam-A.hmm.gz

mkdir ${working_dir}
cd ${working_dir}

blastp_output_file=${working_dir}/blastp.outfmt6 # default for transDecoder
pfam_output_file=${working_dir}/pfam.domtblout # default for transDecoder

longest_orf_pep_file=${working_dir}/${fasta_basename}.transdecoder_dir/longest_orfs.pep

# step 1: Predicting coding regions from a transcript fasta file

if [ ! -f ${longest_orf_pep_file} ]; then
  ${transdecoder_dir}/TransDecoder.LongOrfs -t ${input_fasta_file}
fi

# step 2:  scan all ORFs for homology to known proteins
# step 2.1: a BLAST search against a database of known proteins
if [ ! -f ${blastp_output_file} ]; then
  blastp -query ${longest_orf_pep_file} -db ${BLASTP_DB} -max_target_seqs 1 \
    -outfmt 6 -evalue 1e-5 -num_threads ${N_CPU} >${blastp_output_file}
fi

# step 2.2  : searching PFAM to identify common protein domains
# parameters used for hmmscan
#-o <f>           : direct output to file <f>, not stdout
#--tblout <f>     : save parseable table of per-sequence hits to file <f>
#--domtblout <f>  : save parseable table of per-domain hits to file <f>
#--pfamtblout <f> : save table of hits and domains to file, in Pfam format <f>

if [ ! -f ${pfam_output_file} ]; then
  ${hmmscan_bin} --cpu ${N_CPU} --domtblout ${pfam_output_file}  ${PFAM_DB} ${longest_orf_pep_file}
fi

# step 3: Integrating the Blast and Pfam search results into coding region selection
# the output is ${input_fasta_file}.transdecoder.pep
if [[ ( -f ${pfam_output_file} ) && ( -f ${blastp_output_file} ) ]]; then
  ${transdecoder_dir}/TransDecoder.Predict -t ${input_fasta_file} --retain_pfam_hits ${pfam_output_file} 
--retain_blastp_hits ${blastp_output_file}
fi

