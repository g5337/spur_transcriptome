## Transcriptome Assembly And Comparison To Study The Molecular Mechanism of Nectar Spur Development

## Description
Here are the scripts that were used to analyse/compare the transcriptome of two species within the Antirrhineae (*Linaria vulgaris* with spur and *Antirrhinum Majus* without spur), in order to investigate the molecular mechanism of nectar spur development. The illumina data used in this project is available in the European Nucleotide Archive.

## Authors and acknowledgement
Those scripts were written by Qi Wang, with contributions from Erin Cullen for codes and discussions, and Beverley Glover for supervision of experimental design.

## License
The MIT License (MIT)
Copyright © 2022

## Project status
This is an archive of scripts that were used for analysis of this spur transcriptome projects.









